#!/usr/bin/env ruby

require 'yaml'
require 'gnuplot'

COLORS = %w[black blue green red].freeze

##
## Options
##
OptStruct = Struct.new(:input, :output, keyword_init: true)
OPTIONS = OptStruct.new(
  input: ARGV.shift,
  output: ARGV.shift
)

##
## Functions
##
def dataset(res)
  freq = {}
  res['trace'].each do |trace|
    t = trace.last
    freq[t] ||= 0.0
    freq[t] += 1.0
  end

  count = res['trace'].length.to_f
  freq.transform_values { |x| x / count }
      .sort
      .transpose
end

def main
  raise ArgumentError, 'Input must be defined' unless input
  res = YAML.load_file(input)

  Gnuplot.open do |gp|
    Gnuplot::Plot.new(gp) do |plot|
      if output
        plot.terminal 'png transparent size 1024,600'
        plot.output output
      end

      plot.xrange '[1500:1800]'
      plot.ylabel 'frequency of clustering'
      plot.xlabel 'cost of clustering'
      # plot.style 'data histogram'
      # plot.style 'histogram cluster gap 1'
      plot.style 'fill solid noborder'
      plot.boxwidth '1'

      res.each_with_index do |r, i|
        plot.data << Gnuplot::DataSet.new(dataset(r)) do |ds|
          # ds.title = i
          ds.with = 'boxes'
          # ds.smooth = 'sbezier'
          # ds.using = "1:2 pt 7 ps 2 lc rgb \"#{COLORS[i]}\""
          # ds.using = 'smooth csplines t "cubic smooth" lw 2'
        end
      end

      puts plot.to_gplot
    end
  end
end

##
## Parse Options
##

OPTIONS.instance_eval { main }
