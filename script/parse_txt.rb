#!/usr/bin/env ruby

fname = ARGV[0]
cen_fname = ARGV[1]

exit if fname.nil?

cen_pts = []
if cen_fname
  puts "Parsing #{cen_fname} for solutions"

  File.open cen_fname do |f|
    f.each_line { |l| cen_pts << l.split.map(&:to_f) }
  end
end

puts "Parsing #{fname}"

pts = []
File.open fname do |f|
  f.each_line { |l| pts << l.split.map(&:to_f) }
end

cen_costs = []
unless cen_pts.empty?
  pts.each do |pt|
    pt
  end
end

File.open "#{fname}.vec", 'w' do |f|
  f.puts "#{pts[0].size} #{cen_pts.size} #{pts.size}"

  cen_pts.each_with_index { |pt, i| f.puts "#{pt.join ' '} #{cen_costs[i]}" }
  pts.each { |pt| f.puts pt.join ' ' }
end
