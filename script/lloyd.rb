class Lloyd
  attr_reader :pts, :cts

  def initialize(pts, cts)
    @pts = pts
    @cts = cts
  end

  def iter
    # Distribute points to clusters
    cts = Array.new(@cts.length) { Array.new(@cts.first.length, 0.0) }
    siz = Array.new(@cts.length, 0)
    @pts.each do |pt|
      i = _ct(pt)
      cts[i] = _add(cts[i], @cts[i])
      siz[i] += 1
    end

    STDOUT.putc (@cts == cts ? '.' : '!')
    STDOUT.flush

    # Compute centroids
    cts = cts.zip(siz).map { |c, s| _div(c, s.to_f) }

    # Check if we've converged
    return _cost if @cts == cts

    # We haven't converged
    @cts = cts
    false
  end

  def self.run(pts, cts)
    lloyd = Lloyd.new(pts, cts)
    loop do
      cost = lloyd.iter
      return lloyd.cts, cost if cost
    end
  end

  private

  def _d(a, b)
    a.zip(b)
     .sum { |x, y| (x - y)**2 }
  end

  def _add(a, b)
    a.zip(b).map { |x, y| x + y }
  end

  def _div(a, s)
    a.map { |x| x / s }
  end

  def _ct(pt)
    @cts.map.with_index { |ct, i| [_d(ct, pt), i] }
        .min_by(&:first)
        .last
  end

  def _cost
    @pts.sum { |pt| _d(pt, @cts[_ct(pt)]) }
  end
end
