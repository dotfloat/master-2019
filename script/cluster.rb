#!/usr/bin/env ruby

require 'yaml'
require './txt_format.rb'

INPUT = YAML.load_file(ARGV.shift)

def dist(a, b, &blk)
  a.zip(b).map { |x, y| blk[x, y].to_s(2).count('1') }.sum
end
