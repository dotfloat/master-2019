#!/bin/bash
set -e

if [ "$@" != 1 ]; then
	echo "Syntax: $0 [SOURCE DIR]"
	exit 1
fi

SRC="$1"
TMP="/tmp/master-code"
BIN="${TMP}-prefix"

# Cleanup from last time
rm -rf "${TMP_DIR}"
mkdir -p "${TMP_DIR}"

# Generate meson
meson --static=true --default-library=static --build_type=release --install-prefix="${BIN}" "${SRC}" "${TMP}"

# Compile with ninja
ninja -C "${TMP}" install

# tar-xz
