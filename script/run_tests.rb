#!/usr/bin/env ruby

DIR = ARGV.shift
NPROC = ENV.fetch('NPROC') { `nproc`.chomp }
tests = []

def run_tests(args)
  Dir.glob("#{DIR}/*.txt") do |f|
    command = "#{args} #{f} 2>/dev/null"
    tests << command
  end
end

# Run lloyd
run_tests('./lloyd --round --type=double --cost-eval -i100')

# Run bv_lloyd
run_tests('./bv_lloyd --cost-eval -i100')

COMPLETED = File.read('binary_vectors_commands.txt').split("\n")
REMAINING = TESTS - COMPLETED

puts "Total tests: #{tests.length}"
puts "Completed: #{COMPLETED.length}"
puts "Remaining: #{REMAINING.length}"

threads = []
File.open 'binary_vectors.yaml', 'w' do |fh|
  File.open 'binary_vectors_commands.txt', 'a' do |l|
    NPROC.times do
      THREADS << Thread.new do
        break if REMAINING.empty?

        top = REMAINING.shift
        fh.puts `#{top}`
        l.puts top
        puts "[#{REMAINING.length} left] #{top}"
      end
    end
  end
end
threads.each(&:join)
