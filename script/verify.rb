#!/usr/bin/env ruby

require 'yaml'
require 'optparse'
require_relative 'txt_format.rb'

##
## Options
##

##
## Functions
##
def dist(a, b)
  a.zip(b).sum { |x, y| (x - y) ** 2 }
end

def verify(obj)
  pts = TxtFormat.read_pts(obj['dataset'])
  cts = obj['result']

  total_cost = pts.sum do |pt|
    cts.map { |ct| dist(ct, pt) }.min
  end

  puts "Expected: #{obj['cost']}, Actual: #{total_cost} (#{obj['cost'] == total_cost ? 'OK' : 'FAIL'})"
end

def main
  yaml = YAML.load_file(ARGV.shift)
  yaml.each_with_index do |x, i|
    puts "Checking [#{i + 1}/#{yaml.length}]"
    verify(x)
  end
end

##
## Parse Options
##

main
