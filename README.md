MASTER
======

# Directory Overview

`test` - Contains unit tests

`src/bin` - Binary Vector Library (Chapter 3)

`src/real` - Vector Library (Chapter 2)

`src/lloyd` - The various implementations of Lloyd

`src/kumar` - Efficient Kumar algorithm

`src/fomin` - Partial implementation of Fedor's paper, which we dropped

`src/util` & `src/uti` - Various utilities

`src/common` - Common post-processing algorithms and framework

# Compile Instructions

Install latest versions of `gcc`, `meson` and `boost`.

Create and set up the build directory.
```
$ mkdir build && cd build
$ meson --buildtype=release --default-library=static -Dstatic=true ..
```

To compile the different executables:
```
$ ninja lloyd0f
$ ninja lloyd_bin0z
$ ninja kumar0d
$ ninja lloyd_lr0z
```

To test:
```
$ ninja test
```

We can set the maximum dimension:
```
$ meson override -Dreal_size=16
$ ninja lloyd16f
```