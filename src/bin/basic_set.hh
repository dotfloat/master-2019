#ifndef __BASIC_SET__36090796
#define __BASIC_SET__36090796

#include <algorithm>
#include <numeric>
#include <vector>

#include "traits.hh"

namespace bin {
  template <size_t NBytes, class AOp>
  class basic_set {
  public:
      using traits_type     = traits<NBytes, AOp>;
      using dim_type        = typename traits_type::dim_type;
      using unit_type       = typename traits_type::unit_type;
      using value_type      = typename traits_type::vec_type;
      using reference       = typename traits_type::ref_type;
      using const_reference = typename traits_type::const_ref_type;

      class iterator;
      class const_iterator;

  private:
      dim_type m_dim;
      std::vector<unit_type> m_vec;

  public:
      basic_set() = delete;

      basic_set(const basic_set&) = default;

      basic_set(basic_set&&) noexcept = default;

      basic_set(dim_type dim, size_t count = 0):
          m_dim(dim),
          m_vec(dim.nbytes() * count)
      { assert(dim > 0); }

      explicit basic_set(const std::initializer_list<value_type>& il):
          m_dim(il.begin()->dim()),
          m_vec(il.size() * m_dim.nbytes())
      {
          assert(il.begin()->dim() > 0);
          auto it1 = m_vec.begin();
          auto it2 = il.begin();

          for (; it1 != m_vec.end(); it1 += dim(), ++it2) {
              std::copy(it2->begin(), it2->end(), it1);
          }
      }

      basic_set& operator=(const basic_set&) = default;
      basic_set& operator=(basic_set&&) noexcept = default;

      /**
       * Insertion
       */
      template <class U, typename = enable_if_vec_t<U>>
      void push_back(const U& vec)
      {
          m_vec.insert(m_vec.end(), vec.data(), vec.data() + vec.nbytes());
      }

      void push_back(std::initializer_list<unit_type> vec)
      {
          assert(vec.size() == dim());
          m_vec.insert(m_vec.end(), vec.data(), vec.data() + vec.nbytes());
      }

      /**
       * Reset
       */
      void clear()
      { m_vec.clear(); }

      /**
       * Data
       */
      bool empty() const
      { return m_vec.empty(); }

      /**
       * Size
       */
      dim_type dim() const
      { return m_dim; }

      size_t size() const
      { assert(m_dim > 0); return m_vec.size() / m_dim.nbytes(); }

      /**
       * Accessors
       */
      reference front()
      { return (*this)[0]; }

      const_reference front() const
      { return (*this)[0]; }

      reference back()
      { return (*this)[size() - 1]; }

      const_reference back() const
      { return (*this)[size() - 1]; }

      reference operator[](size_t i)
      { return { m_vec.data() + i * m_dim.nbytes(), m_dim }; }

      const_reference operator[](size_t i) const
      { return { m_vec.data() + i * m_dim.nbytes(), m_dim }; }

      /**
       * Comparators
       */
      bool operator==(const basic_set& other) const
      { return m_vec == other.m_vec; }

      bool operator!=(const basic_set& other) const
      { return m_vec != other.m_vec; }

      /**
       * Iterators
       */
      iterator begin()
      { return iterator(m_vec.data(), m_dim); }

      const_iterator begin() const
      { return { m_vec.data(), m_dim }; }

      const_iterator cbegin() const
      { return { m_vec.data(), m_dim }; }

      iterator end()
      { return { m_vec.data() + m_vec.size(), m_dim }; }

      const_iterator end() const
      { return { m_vec.data() + m_vec.size(), m_dim }; }

      const_iterator cend() const
      { return { m_vec.data() + m_vec.size(), m_dim }; }

      /**! Compute Frobenius norm */
      unit_type norm()
      {
          return std::inner_product(
              m_vec.begin(), m_vec.end(), m_vec.begin(), 0_sz,
              std::plus<size_t> {},
              [](const size_t& a, const size_t& b) {
                  return __builtin_popcountll(a ^ b);
              });
      }
  };

  template <size_t NBytes, class AOp>
  class basic_set<NBytes, AOp>::iterator {
  public:
      using traits_type       = traits<NBytes, AOp>;
      using unit_type         = typename traits_type::unit_type;
      using value_type        = typename traits_type::vec_type;
      using dim_type          = typename traits_type::dim_type;
      using reference         = typename traits_type::ref_type;
      using difference_type   = std::ptrdiff_t;
      using const_reference   = std::add_const_t<reference>;
      using pointer           = unit_type *;
      using const_pointer     = const unit_type *;
      using iterator_category = std::random_access_iterator_tag;

  private:
      dim_type m_dim;
      pointer m_iter;
      reference m_ref;

  public:
      iterator() = default;
      iterator(const iterator&) = default;
      iterator(iterator&&) noexcept = default;

      iterator(pointer iter, dim_type dim):
          m_dim(dim),
          m_iter(iter),
          m_ref(m_iter, m_dim)
      { assert(m_dim > 0); }

      iterator& operator=(const iterator& other)
      {
          m_dim  = other.m_dim;
          m_iter = other.m_iter;
          m_ref.reset(m_iter, m_dim);
          return *this;
      }

      iterator& operator=(iterator&& other) noexcept
      {
          m_dim = other.m_dim;
          m_iter = other.m_iter;
          m_ref.reset(m_iter, m_dim);
          return *this;
      }

      /* Accessors */

      reference& operator*()
      { return m_ref; }

      const_reference& operator*() const
      { return m_ref; }

      reference* operator->()
      { return &m_ref; }

      const_reference* operator->() const
      { return &m_ref; }

      /* Iterator Operators */

      iterator& operator++()
      {
          m_iter += m_dim.nbytes();
          m_ref.reset(m_iter);
          return *this;
      }

      iterator operator++(int)
      {
          auto copy = *this;
          ++*this;
          return copy;
      }

      iterator& operator--()
      {
          m_iter -= m_dim.nbytes();
          m_ref.reset(m_iter);
          return *this;
      }

      iterator operator--(int)
      {
          auto copy = *this;
          --*this;
          return copy;
      }

      iterator operator+(difference_type diff) const
      {
          auto copy = *this;
          copy += diff;
          return copy;
      }

      iterator& operator+=(difference_type diff)
      {
          m_iter += diff * m_dim.nbytes();
          m_ref.reset(m_iter);
          return *this;
      }

      iterator operator-(difference_type diff) const
      {
          auto copy = *this;
          copy -= diff;
          return copy;
      }

      iterator& operator-=(difference_type diff)
      {
          m_iter -= diff * m_dim.nbytes();
          m_ref.reset(m_iter);
          return *this;
      }

      difference_type operator-(const iterator& other) const
      { return (m_iter - other.m_iter) / m_dim.nbytes(); }

      bool operator!=(const iterator& other) const
      { return m_iter != other.m_iter; }
  };

  template <size_t NBytes, class AOp>
  class basic_set<NBytes, AOp>::const_iterator {
  public:
      using traits_type       = traits<NBytes, AOp>;
      using unit_type         = typename traits_type::unit_type;
      using value_type        = typename traits_type::vec_type;
      using dim_type          = typename traits_type::dim_type;
      using reference         = typename traits_type::const_ref_type;
      using difference_type   = std::ptrdiff_t;
      using const_reference   = std::add_const_t<reference>;
      using pointer           = const unit_type *;
      using const_pointer     = pointer;
      using iterator_category = std::random_access_iterator_tag;

  private:
      dim_type m_dim;
      pointer m_iter;
      reference m_ref;

  public:
      const_iterator() = default;
      const_iterator(const const_iterator&) = default;
      const_iterator(const_iterator&&) noexcept = default;

      const_iterator(const iterator& other):
          m_dim(other.m_dim),
          m_iter(other.m_iter),
          m_ref(m_iter, m_dim)
      {}

      const_iterator(pointer iter, dim_type dim):
          m_dim(dim),
          m_iter(iter),
          m_ref(m_iter, m_dim)
      { assert(m_dim > 0); }

      const_iterator& operator=(const const_iterator& other)
      {
          m_dim  = other.m_dim;
          m_iter = other.m_iter;
          m_ref.reset(m_iter, m_dim);
          return *this;
      }

      const_iterator& operator=(const iterator& other)
      {
          m_dim  = other.m_dim;
          m_iter = other.m_iter;
          m_ref.reset(m_iter, m_dim);
          return *this;
      }

      const_iterator& operator=(const_iterator&& other) noexcept
      {
          m_dim = other.m_dim;
          m_iter = other.m_iter;
          m_ref.reset(m_iter, m_dim);
          return *this;
      }

      /* Accessors */

      const_reference& operator*() const
      { return m_ref; }

      const_reference* operator->() const
      { return &m_ref; }

      /* Iterator Operators */

      const_iterator& operator++()
      {
          m_iter += m_dim.nbytes();
          m_ref.reset(m_iter);
          return *this;
      }

      const_iterator operator++(int)
      {
          auto copy = *this;
          ++*this;
          return copy;
      }

      const_iterator& operator--()
      {
          m_iter -= m_dim.nbytes();
          m_ref.reset(m_iter);
          return *this;
      }

      const_iterator operator--(int)
      {
          auto copy = *this;
          --*this;
          return copy;
      }

      const_iterator operator+(difference_type diff) const
      {
          auto copy = *this;
          copy += diff;
          return copy;
      }

      const_iterator& operator+=(difference_type diff)
      {
          m_iter += diff * m_dim.nbytes();
          m_ref.reset(m_iter);
          return *this;
      }

      const_iterator operator-(difference_type diff) const
      {
          auto copy = *this;
          copy -= diff;
          return copy;
      }

      const_iterator& operator-=(difference_type diff)
      {
          m_iter -= diff * m_dim.nbytes();
          m_ref.reset(m_iter);
          return *this;
      }

      difference_type operator-(const iterator& other) const
      { return (m_iter - other.m_iter) / m_dim.nbytes(); }

      difference_type operator-(const const_iterator& other) const
      { return (m_iter - other.m_iter) / m_dim.nbytes(); }

      bool operator!=(const iterator& other) const
      { return m_iter != other.m_iter; }

      bool operator!=(const const_iterator& other) const
      { return m_iter != other.m_iter; }
  };
}

#endif //__BASIC_SET__36090796
