#ifndef __UNIT__94247754
#define __UNIT__94247754

#include <istream>
#include <cassert>

namespace bin {
  class unit {
      int m_val {};

      friend constexpr bool operator==(const unit&, const unit&);
      friend constexpr bool operator!=(const unit&, const unit&);
      friend constexpr bool operator>(const unit&, const unit&);
      friend constexpr bool operator<(const unit&, const unit&);
      friend constexpr bool operator>=(const unit&, const unit&);
      friend constexpr bool operator<=(const unit&, const unit&);
      friend constexpr unit operator-(const unit&);
      friend constexpr unit operator+(const unit&);
      friend constexpr unit operator-(const unit&, const unit&);
      friend constexpr unit operator+(const unit&, const unit&);
      friend constexpr unit operator*(const unit&, const unit&);
      friend constexpr unit operator/(const unit&, const unit&);
      friend constexpr unit operator%(const unit&, const unit&);

  public:
      constexpr unit() = default;
      constexpr unit(const unit&) = default;

      constexpr explicit unit(int val):
          m_val(val)
      { assert(val == 0 || val == 1); }

      constexpr unit& operator=(const unit&) = default;

      constexpr unit& operator=(int val)
      {
          assert(val == 0 || val == 1);
          m_val = val;
          return *this;
      }

      constexpr operator int() const
      { return m_val; }

      constexpr unit& operator+=(const unit& x)
      {
          m_val ^= x.m_val;
          return *this;
      }

      constexpr unit& operator-=(const unit& x)
      {
          m_val ^= x.m_val;
          return *this;
      }

      constexpr unit& operator*=(const unit& x)
      {
          m_val &= x.m_val;
          return *this;
      }

      constexpr unit& operator/=(const unit& x)
      {
          assert(x.m_val != 0);
          return *this;
      }

      constexpr unit& operator%=(const unit& x)
      {
          assert(x.m_val != 0);
          m_val = 0;
          return *this;
      }
  };

  constexpr bool operator==(const unit& a, const unit& b)
  { return a.m_val == b.m_val; }

  constexpr bool operator!=(const unit& a, const unit& b)
  { return a.m_val != b.m_val; }

  constexpr bool operator<(const unit& a, const unit& b)
  { return a.m_val < b.m_val; }

  constexpr bool operator>(const unit& a, const unit& b)
  { return a.m_val > b.m_val; }

  constexpr bool operator<=(const unit& a, const unit& b)
  { return a.m_val <= b.m_val; }

  constexpr bool operator>=(const unit& a, const unit& b)
  { return a.m_val >= b.m_val; }

  constexpr unit operator!(const unit& a)
  { return unit { !static_cast<int>(a) }; }

  constexpr unit operator+(const unit& a)
  { return a; }

  constexpr unit operator-(const unit& a)
  { return a; }

  constexpr unit operator+(const unit& a, const unit& b)
  { return unit { a.m_val ^ b.m_val }; }

  constexpr unit operator-(const unit& a, const unit& b)
  { return unit { a.m_val ^ b.m_val }; }

  /** a ✕ b = 1 iff. both a and b = 1. 0 otherwise. */
  constexpr unit operator*(const unit& a, const unit& b)
  { return unit { a.m_val & b.m_val }; }

  /** a / b is only defined if b ≠ 0. Since it's UB to divide by zero, we can
   * simply return a */
  constexpr unit operator/(const unit& a, const unit& b)
  {
      assert(b.m_val != 0);
      return a;
  }

  /** a % b is only defined if b ≠ 0, and both 0 and 1 = 0 (mod 1), simply return 0. */
  constexpr unit operator%(const unit&, const unit& b)
  {
      assert(b.m_val != 0);
      return {};
  }
}

inline std::istream& operator>>(std::istream& s, ::bin::unit& r)
{
    int i;
    s >> i;
    r = i & 1;
    return s;
}

#endif //__UNIT__94247754
