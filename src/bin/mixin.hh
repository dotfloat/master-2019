#ifndef __MIXIN__26706336
#define __MIXIN__26706336

#include <cstddef>
#include <functional>
#include <numeric>

#include <prelude.hh>
#include "traits.hh"

namespace bin {
  template <class>
  struct mixin_types {};

  template <class Self, size_t NBytes, class AOp>
  class mixin : public mixin_types<Self> {
  public:
      using traits_type            = typename mixin_types<Self>::traits_type;
      using dim_type               = typename mixin_types<Self>::dim_type;
      using value_type             = typename mixin_types<Self>::value_type;
      using pointer                = typename mixin_types<Self>::pointer;
      using const_pointer          = typename mixin_types<Self>::const_pointer;
      using reference              = typename mixin_types<Self>::reference;
      using const_reference        = typename mixin_types<Self>::const_reference;
      using iterator               = typename mixin_types<Self>::iterator;
      using const_iterator         = typename mixin_types<Self>::const_iterator;
      using reverse_iterator       = typename mixin_types<Self>::reverse_iterator;
      using const_reverse_iterator = typename mixin_types<Self>::const_reverse_iterator;

  private:
      friend class basic_vec<NBytes>;
      friend class basic_vec<NBytes>;
      template <class, size_t, class>
      friend class mixin;

      using self = Self;
      using owning = basic_vec<NBytes>;

      self& m_self()
      { return *static_cast<self*>(this); }

      const self& m_self() const
      { return *static_cast<const self*>(this); }

      size_t *m_blocks_begin()
      { return m_self().data(); }

      const size_t *m_blocks_begin() const
      { return m_self().data(); }

      size_t *m_blocks_end()
      { return m_self().data() + this->nbytes(); }

      const size_t *m_blocks_end() const
      { return m_self().data() + this->nbytes(); }

      template <class U, class Func>
      self& m_transform(const U& vec, Func&& func)
      {
          std::transform(
              m_blocks_begin(),
              m_blocks_end(),
              vec.m_blocks_begin(),
              m_blocks_begin(),
              std::forward<Func>(func));
          return m_self();
      }

      template <class Func>
      self& m_transform(Func&& func)
      {
          std::transform(m_blocks_begin(), m_blocks_end(), m_blocks_begin(), std::forward<Func>(func));
          return m_self();
      }

      template <class U, class Func>
      owning m_transform_copy(const U& vec, Func&& func) const
      {
          owning cpy = m_self();
          cpy.m_transform(vec, std::forward<Func>(func));
          return cpy;
      }

      template <class Func>
      owning m_transform_copy(Func&& func) const
      {
          owning cpy = m_self();
          cpy.m_transform(std::forward<Func>(func));
          return cpy;
      }

  public:
      /**
       * Vector Boolean Operators
       */
      template <class U>
      bool operator==(const U& vec) const
      {
          for (size_t i {}; i < (dim() + sizeof(size_t) - 1) / sizeof(size_t); ++i) {
              if (data()[i] != vec.data()[i]) {
                  return false;
              }
          }
          return true;
      }

      template <class U>
      bool operator!=(const U& vec) const
      { return !(*this == vec); }

      /**
       * Vector Arithmetic Operators
       */
      template <class U, typename = std::enable_if_t<is_bin_v<U>>>
      self& operator+=(const U& vec)
      { return m_transform(vec, AOp {}); }

      template <class U, typename = std::enable_if_t<is_bin_v<U>>>
      self& operator-=(const U& vec)
      { return m_transform(vec, AOp {}); }

      template <class U, typename = std::enable_if_t<is_bin_v<U>>>
      self& operator*=(const U& vec)
      {
          if (static_cast<int>(vec) == 0) {
              std::fill_n(data(), dim() / sizeof(size_t), 0);
          }
          return *this;
      }

      template <class U>
      self& operator/=(const U& vec) const
      {
          assert(static_cast<int>(vec) != 0);
          return *this;
      }

      template <class U, typename = std::enable_if_t<is_bin_v<U>>>
      owning operator+(const U& vec) const
      { return m_transform_copy(vec, AOp {}); }

      template <class U, typename = std::enable_if_t<is_bin_v<U>>>
      owning operator-(const U& vec) const
      { return m_transform_copy(vec, AOp {}); }

      owning operator*(const unit& u) const
      {
          if (static_cast<int>(u) == 1) {
              return m_self();
          } else {
              return owning(dim());
          }
      }

      template <bool C>
      owning operator*(const unit_ref<C>& u) const
      { return *this * static_cast<unit>(u); }

      owning operator-() const
      { return m_self(); }

      owning operator~() const
      {
          auto mask = ~((~0_sz) << (nbits() % sizeof(size_t)));
          auto cpy = m_transform_copy([](const size_t& x) { return ~x; });
          *(cpy.m_blocks_end() - 1) &= mask;
          return cpy;
      }

      /**
       * Data access
       */
      pointer data()
      { return m_self().p_data(); }

      const_pointer data() const
      { return m_self().p_data(); }

      dim_type dim() const
      { return m_self().p_dim(); }

      size_t nbits() const
      { return dim().nbits(); }

      size_t nbytes() const
      { return dim().nbytes(); }

      size_t size() const
      { return nbits(); }

      /**
       * Accessors
       */
      reference operator[](size_t idx)
      { return reference { data() + idx / (sizeof(size_t) * 8), idx % (sizeof(size_t) * 8) }; }

      const_reference operator[](size_t idx) const
      { return const_reference { data() + idx / (sizeof(size_t) * 8), idx % (sizeof(size_t) * 8) }; }

      reference front()
      { return (*this)[0]; }

      const_reference front() const
      { return (*this)[0]; }

      reference back()
      { return (*this)[nbits() - 1]; }

      const_reference back() const
      { return (*this)[nbits() - 1]; }

      /**
       * Iterators
       */
      iterator begin()
      { return { data(), 0 }; }

      const_iterator begin() const
      { return { data(), 0 }; }

      const_iterator cbegin() const
      { return { data(), 0 }; }

      iterator end()
      { return { data() + dim() / sizeof(size_t), dim() % sizeof(size_t) }; }

      const_iterator end() const
      { return { data() + dim() / sizeof(size_t), dim() % sizeof(size_t) }; }

      const_iterator cend() const
      { return { data() + dim() / sizeof(size_t), dim() % sizeof(size_t) }; }

      reverse_iterator rbegin()
      { return end(); }

      const_reverse_iterator rbegin() const
      { return end(); }

      const_reverse_iterator crbegin() const
      { return end(); }

      reverse_iterator rend()
      { return begin(); }

      const_reverse_iterator rend() const
      { return begin(); }

      const_reverse_iterator crend() const
      { return begin(); }

      /**
       * Functions
       */
      size_t norm() const
      {
          size_t n {};
          for (auto it = m_blocks_begin(); it != m_blocks_end(); ++it) {
              n += __builtin_popcountll(*it);
          }
          return n;
      }

      /* Proper hamming distance */
      template <class U, typename = enable_if_vec_t<U>>
      size_t dist(const U& u) const
      {
          size_t n {};
          for (auto it1 = m_blocks_begin(), it2 = u.m_blocks_begin(); it1 != m_blocks_end(); ++it1, ++it2) {
              n += __builtin_popcountll(*it1 ^ *it2);
          }
          return n;
      }
  };
}

#endif //__MIXIN__26706336
