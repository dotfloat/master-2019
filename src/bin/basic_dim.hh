#ifndef __BASIC_DIM__69416036
#define __BASIC_DIM__69416036

#include <stdexcept>
#include <cstddef>

#include <uti/helper.hh>

namespace bin {
  constexpr size_t nbytes(size_t nbits)
  { return uti::divceil(nbits, sizeof(size_t) * 8); }

  struct basic_dim_error : std::logic_error { using std::logic_error::logic_error; };

  constexpr size_t Dynamic = 0;

  template <size_t NBytes>
  class basic_dim {
      size_t m_nbits;

  public:
      basic_dim() = delete;

      constexpr basic_dim(size_t nbits):
          m_nbits(nbits)
      {
          if (::bin::nbytes(nbits) == NBytes)
              return;
          throw basic_dim_error { "Invalid basic_dimension size" };
      }

      template <size_t M>
      constexpr basic_dim(const basic_dim<M>& other):
          m_nbits(other.nbits())
      {
          if (other.nbytes() == NBytes)
              return;
          throw basic_dim_error { "Invalid basic_dimension size" };
      }

      constexpr basic_dim& operator=(const basic_dim&) = default;

      constexpr operator size_t() const
      { return m_nbits; }

      constexpr size_t nbits() const
      { return m_nbits; }

      constexpr size_t nbytes() const
      { return NBytes; }
  };

  template <>
  class basic_dim<Dynamic> {
      size_t m_nbits;

  public:
      basic_dim() = delete;

      constexpr basic_dim(size_t nbits):
          m_nbits(nbits)
      { assert(m_nbits > 0); }

      template <size_t N>
      constexpr basic_dim(const basic_dim<N>& other):
          m_nbits(other.nbits())
      { assert(m_nbits > 0); }

      constexpr basic_dim& operator=(const basic_dim&) = default;

      constexpr operator size_t() const
      { return m_nbits; }

      constexpr size_t nbits() const
      { return m_nbits; }

      constexpr size_t nbytes() const
      { return ::bin::nbytes(m_nbits); }
  };
}

#endif //__BASIC_DIM__69416036
