#ifndef __TRAITS__93997195
#define __TRAITS__93997195

#include <type_traits>
#include <cstddef>
#include "fwd.hh"
#include "basic_dim.hh"

namespace bin {
/**
 * Define traits
 */
  template <size_t NBytes, class AOp>
  struct traits {
      using dim_type       = basic_dim<NBytes>;
      using unit_type      = size_t;
      using vec_type       = basic_vec<NBytes, AOp>;
      using ref_type       = basic_ref<NBytes, AOp>;
      using const_ref_type = basic_view<NBytes, AOp>;
      using set_type       = basic_set<NBytes, AOp>;
  };
}

#endif //__TRAITS__93997195
