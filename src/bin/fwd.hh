#ifndef __FWD__93043252
#define __FWD__93043252

#include <cstddef>
#include <functional>

namespace bin {
  class unit;
  template <bool> class unit_ref;
  template <bool> class unit_iter;
  template <size_t N = 0, class A = std::bit_xor<size_t>> class basic_vec;
  template <size_t N = 0, class A = std::bit_xor<size_t>> class basic_ref;
  template <size_t N = 0, class A = std::bit_xor<size_t>> class basic_view;
  template <size_t N = 0, class A = std::bit_xor<size_t>> class basic_set;

  template <class>
  struct is_bin : std::false_type {};

  template <size_t N, class A>
  struct is_bin<basic_vec<N, A>> : std::true_type {};

  template <size_t N, class A>
  struct is_bin<basic_ref<N, A>> : std::true_type {};

  template <size_t N, class A>
  struct is_bin<basic_view<N, A>> : std::true_type {};

  template <class T>
  constexpr auto is_bin_v = is_bin<T>::value;

  template <class T>
  using enable_if_vec_t = std::enable_if_t<is_bin_v<T>>;

  template <class T>
  struct is_bin_set : std::false_type {};

  template <size_t N, class A>
  struct is_bin_set<basic_set<N, A>> : std::true_type {};

  template <class T>
  constexpr auto is_bin_set_v = is_bin_set<T>::value;
}

#endif //__FWD__93043252
