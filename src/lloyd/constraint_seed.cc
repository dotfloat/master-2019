#include <random>
#include "low_rank.hh"
#include <common/options.hh>
#include <fmt/core.h>

void lloyd::constraint_seed(lloyd::aux& aux, const std::vector<size_t>& r)
{
    auto& cur = aux.curr_clusters();
    std::uniform_int_distribution rand_row { 0_sz, r.size() - 1 };

    for (size_t i {}; i < aux.dim(); ++i) {
        auto which = rand_row(common::g_gen);
        auto row = r[which];

        for (size_t j {}; j < aux.num_clusters(); ++j) {
            cur[j][i] = bin::unit { (row >> j) & 1 };
        }
    }
}
