#include "low_rank.hh"

void lloyd::greedy_low_rank(lloyd::aux& aux, size_t rank)
{
    using closest_type = util::result_cost<size_t, size_t>;

    struct cluster {
        size_t index {};
        size_t size {};
        size_t cost {};
        std::vector<size_t> hamming_weight;

        cluster(const lloyd::aux& aux):
            hamming_weight(aux.dim(), 0_sz)
        {}
    };

    std::vector<cluster> clusters { aux.num_clusters(), cluster { aux } };

    /* Set indices */
    for (size_t i {}; i < clusters.size(); ++i) {
        clusters[i].index = i;
    }

    /* Count the sizes of partitions */
    for (const auto& pt : aux.points()) {
        /* Find the the point's cluster O(dk) */
        closest_type closest {};
        for (size_t j {}; j < aux.num_clusters(); ++j) {
            const auto& c = aux.curr_clusters()[j];
            closest.try_assign(j, cost::hamming {}(pt, c));
        }

        /* Update it */
        auto& cluster = clusters[closest.get()];

        ++cluster.size;
        for (size_t i {}; i < aux.dim(); ++i) {
            if (pt[i] != 0) {
                ++cluster.hamming_weight[i];
                ++cluster.cost;
            }
        }
    }

    auto ccost = [&](size_t ci, auto v) {
        size_t f {};
        /* Compute the cost */
        const auto& cluster = clusters[ci];
        for (size_t i {}; i < cluster.hamming_weight.size(); ++i) {
            if (v[i]) {
                f += cluster.size * cluster.hamming_weight[i];
            } else {
                f += cluster.size * (cluster.size - cluster.hamming_weight[i]);
            }
        }
        return f;
    };

    /* Select the largest clusters */
    std::vector<cluster> rclusters { rank, cluster { aux } };
    std::partial_sort_copy(
        clusters.begin(), clusters.end(),
        rclusters.begin(), rclusters.end(),
        [](const auto& a, const auto& b) {
            return a.cost < b.cost;
        });
    for (size_t i {}; i < rank; ++i) {
        aux.next_clusters()[i] = aux.curr_clusters()[rclusters[i].index];
    }

    /* Select next */
    for (size_t i { rank }; i < aux.num_clusters(); ++i) {
        auto next = aux.next_clusters()[i];

        std::fill(next.begin(), next.end(), 0);
        for (size_t j {}; j < rank; ++j) {
            const auto& ct = aux.curr_clusters()[j];

            if (ccost(i, ct + next) < ccost(i, next)) {
                next += ct;
            }
        }
    }

    /* Calculate the cost of the partitions */
    for (const auto& pt : aux.points()) {
        closest_type closest;
        for (size_t j {}; j < aux.num_clusters(); ++j) {
            const auto& c = aux.next_clusters()[j];
            closest.try_assign(j, pt.dist(c));
        }
        aux.next_cost() += closest.cost();
    }

    aux.end_iter();
}
