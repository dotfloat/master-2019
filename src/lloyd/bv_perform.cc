#include "config.hh"
#include "perform.hh"

#include "common/options.hh"

void lloyd::bv_perform(lloyd::aux& aux)
{
    using unit_type = typename cc::unit;
    using closest_type = util::result_cost<size_t, unit_type>;
    std::vector<std::vector<size_t>> num_ones(
        aux.num_clusters(),
        std::vector<size_t>(aux.points().dim()));

    /* Add up the vectors in each cluster partition. Also calculate the size of
     * each partition */
    for (const auto& pt : aux.points()) {
        closest_type closest;
        for (size_t j {}; j < aux.num_clusters(); ++j) {
            const auto &c = aux.curr_clusters()[j];
            closest.try_assign(j, pt.dist(c));
        }
        auto& no = num_ones[closest.get()];
        for (size_t j {}; j < pt.dim(); ++j) {
            no[j] += pt[j];
        }
        aux.sizes()[closest.get()]++;
    }

    /* Find centroid of each partition by taking the sum of vectors in each
     * partition and dividing it by the size */
    for (size_t i {}; i < aux.num_clusters(); ++i) {
        const auto& siz = aux.sizes()[i];
        const auto& no = num_ones[i];
        auto ct = aux.next_clusters()[i];

        /* If this cluster is empty, select a random point from P */
        if (siz == 0) {
            auto k = std::uniform_int_distribution { 0_sz, aux.points().size() - 1_sz }(common::g_gen);
            ct = aux.points()[k];
            continue;
        }

        for (size_t j {}; j < ct.dim(); ++j) {
            ct[j] = (2*no[j] >= siz) ? 1 : 0;
        }
    }

    aux.end_iter();
}
