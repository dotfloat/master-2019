#ifndef __LOW_RANK__85084840
#define __LOW_RANK__85084840

#include "aux.hh"
#include "util/constraints.hh"

namespace lloyd {
  inline void low_rank(lloyd::aux& aux, const std::vector<size_t>& r);
  void constraint_seed(lloyd::aux& aux, const std::vector<size_t>& r);
  void greedy_low_rank(lloyd::aux& aux, size_t rank);
}

inline void lloyd::low_rank(lloyd::aux& aux, const std::vector<size_t>& r)
{
    using closest_type = util::result_cost<size_t, size_t>;

    struct cluster {
        size_t size {};
        std::vector<size_t> hamming_weight;

        cluster(const lloyd::aux& aux):
            hamming_weight(aux.dim(), 0_sz)
        {}
    };

    /* Count the sizes of partitions */
    std::vector<cluster> clusters { aux.num_clusters(), cluster { aux } };
    for (const auto& pt : aux.points()) {
        /* Find the the point's cluster O(dk) */
        closest_type closest {};
        for (size_t j {}; j < aux.num_clusters(); ++j) {
            const auto& c = aux.curr_clusters()[j];
            closest.try_assign(j, pt.dist(c));
        }

        /* Update it */
        auto& cluster = clusters[closest.get()];

        ++cluster.size;
        for (size_t i {}; i < aux.dim(); ++i) {
            if (pt[i] != 0) {
                ++cluster.hamming_weight[i];
            }
        }
    }

    /* Select the next clusters */
    for (size_t i {}; i < aux.dim(); ++i) {
        util::result_cost<size_t, size_t> best;

        /* Use equation 13 (p. 16) in Fomin et. al that says:
         *
         * f(b_1, ..., b_k) = \sum_{j \in I_b} w_j \cdot z^{(i)} (S_j) + \sum_{j \in [k] \ I_b} w_j \cdot d^{(i)} (S_j)
         *
         * Note however that I_b = [k] and therefore [k] \ I_b = \emptyset. This means that the measure can be reduced to:
         *
         * f(b_1, ..., b_k) = \sum_{j \in [k]} w_j \cdot z^{(i)} (S_j)
         */

        for (const auto& b : r) {
            size_t f {};
            /* Compute the cost */
            for (size_t j {}; j < aux.num_clusters(); ++j) {
                const auto& cluster = clusters[j];
                if (!((b >> j) & 1)) {
                    f += cluster.size * cluster.hamming_weight[i];
                } else {
                    f += cluster.size * (cluster.size - cluster.hamming_weight[i]);
                }
            }
            best.try_assign(b, f);
        }

        auto r = best.get();
        for (size_t j {}; j < aux.num_clusters(); ++j) {
            aux.next_clusters()[j][i] = (r >> j) & 1;
        }
    }

    aux.end_iter();
}


#endif //__LOW_RANK__85084840
