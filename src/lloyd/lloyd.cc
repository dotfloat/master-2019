#include <string_view>
#include <string>
#include <fstream>

#include "common/start.hh"
#include "common/cost.hh"
#include "common/post_process.hh"

#include "lloyd.hh"

#if defined(LOW_RANK) || defined(GREEDY_LOW_RANK)
#include "util/constraints.hh"
#include "low_rank.hh"
#endif

namespace {
  size_t s_iterations = 1;
  bool s_lloydpp {};
  bool s_trace {};
  bool s_iter {};
  std::vector<std::vector<cc::unit>> s_trace_vector;
  std::vector<size_t> s_iter_vector;

  //
  // Lloyd
  //
  class s_lloyd : public common::algorithm<s_lloyd> {
      std::vector<std::vector<cc::unit>> m_trace;
      std::vector<size_t> m_iter;

  public:
      cc::result trial(const cc::set& points) override {
#if defined(LOW_RANK) || defined(GREEDY_LOW_RANK)
          size_t k = 1 << common::g_options.rank;
          lloyd::aux aux(k, points);
#else
          lloyd::aux aux(common::g_options.num_clusters, points);
#endif

#if LOW_RANK
          auto r = uti::make_constraints(common::g_options.rank);
          lloyd::constraint_seed(aux, r);
#else
          if (s_lloydpp) {
              lloyd::furthest_seed(aux);
          } else {
              lloyd::random_seed(aux);
          }
#endif

          auto& trace = m_trace.emplace_back();
          for (size_t i {}; i < s_iterations; ++i) {
#if NORMAL
              lloyd::perform(aux);
#elif BINARY_VECTOR
              lloyd::bv_perform(aux);
#elif LOW_RANK
              lloyd::lr_perform(aux, r);
#elif GREEDY_LOW_RANK
              lloyd::bv_perform(aux);
              lloyd::greedy_low_rank(aux, s_rank);
#endif

              if (s_trace) {
                  auto res = aux.result();
                  common::post_process(aux.points(), res);
                  trace.push_back(res.cost());
              }

              if (aux.converged()) {
                  break;
              }
          }

          if (s_iter) {
              m_iter.push_back(aux.num_iter());
          }

          return aux.result();
      }

      std::string_view name() const override {
          return
#if NORMAL
          "lloyd";
#elif BINARY_VECTOR
          "bv_lloyd";
#elif LOW_RANK
          "lr_lloyd";
#elif GREEDY_LOW_RANK
          "glr_lloyd";
#endif
      }

      void post_thread() const override
      {
          s_trace_vector.insert(s_trace_vector.end(), m_trace.begin(), m_trace.end());
          s_iter_vector.insert(s_iter_vector.end(), m_iter.begin(), m_iter.end());
      }

      void post_program(const yaml::dict& yy) const override
      {
          if (s_trace) {
              auto y = yy.subarray("trace");
              for (const auto& trace : s_trace_vector) {
                  y.inline_array(trace.begin(), trace.end());
              }
          }

          if (s_iter) {
              auto y = yy.subarray("iter");
              y.inline_array(s_iter_vector.begin(), s_iter_vector.end());
          }
      }
  };
}

//
// main
//
int main(int argc, char **argv)
{
    namespace po = boost::program_options;

    po::options_description desc("Lloyd options");
    desc.add_options()
        (",i", po::value(&s_iterations), "Number of iterations per trial")
        ("++", po::bool_switch(&s_lloydpp), "Lloyd++")
        ("trace", po::bool_switch(&s_trace), "Trace costs")
        ("iter", po::bool_switch(&s_iter), "Print number of iterations")
        ;

    common::setup(argc, argv, desc);
    common::start(s_lloyd {});
}
