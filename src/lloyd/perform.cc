#include "config.hh"
#include "perform.hh"

#include "common/options.hh"
#include <uti/min.hh>

void lloyd::perform(lloyd::aux& aux)
{
    using unit_type = typename cc::unit;
    using closest_type = util::result_cost<size_t, unit_type>;

    /* Add up the vectors in each cluster partition. Also calculate the size of
     * each partition */
    for (const auto& pt : aux.points()) {
        auto i = uti::argmin_pt(aux.curr_clusters(), pt);
        aux.next_clusters()[i] += pt;
        aux.sizes()[i]++;
    }

    /* Find centroid of each partition by taking the sum of vectors in each
     * partition and dividing it by the size */
    for (size_t j {}; j < aux.num_clusters(); ++j) {
        /* If this cluster is empty, select a random point from P */
        if (aux.sizes()[j] == 0) {
            auto k = std::uniform_int_distribution { 0_sz, aux.points().size() - 1_sz }(common::g_gen);
            aux.next_clusters()[j] = aux.points()[k];
            continue;
        }

        aux.next_clusters()[j] /= aux.sizes()[j];
    }

    aux.end_iter();
}
