#ifndef __DEFAULT_CONFIG__16540801
#define __DEFAULT_CONFIG__16540801

#ifndef C_REAL_DIM
# define C_REAL_DIM real::Dynamic
#endif

#ifndef C_REAL_TYPE
# define C_REAL_TYPE double
#endif

#ifndef C_BIN_DIM
# define C_BIN_DIM bin::Dynamic
#endif

#ifndef C_BIN_AOP
# define C_BIN_AOP std::bit_xor<size_t>
#endif

#endif //__DEFAULT_CONFIG__16540801
