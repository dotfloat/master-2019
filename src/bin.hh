#ifndef __BIN__71385563
#define __BIN__71385563

#include "bin/unit.hh"
#include "bin/basic_vec.hh"
#include "bin/basic_ref.hh"
#include "bin/basic_view.hh"
#include "bin/basic_set.hh"
#include "bin/format.hh"

#include "default_config.hh"

namespace bin {
  using vec  = basic_vec<C_BIN_DIM, C_BIN_AOP>;
  using ref  = basic_ref<C_BIN_DIM, C_BIN_AOP>;
  using view = basic_ref<C_BIN_DIM, C_BIN_AOP>;
  using set  = basic_set<C_BIN_DIM, C_BIN_AOP>;
  using dim  = basic_dim<C_BIN_DIM>;
}

#endif //__BIN__71385563
