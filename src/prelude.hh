#ifndef __PRELUDE__95403838
#define __PRELUDE__95403838

#include <cstdint>
#include <uti/debug.hh>

/**
 * Define helper functions for integer literals.
 */

#define _PRELUDE_INTLITERALS(_Ty, _Suf)                                        \
    constexpr _Ty operator"" _Suf(unsigned long long int x) \
    { return static_cast<_Ty>(x); }

_PRELUDE_INTLITERALS(std::int8_t,   _i8)
_PRELUDE_INTLITERALS(std::int16_t,  _i16)
_PRELUDE_INTLITERALS(std::int32_t,  _i32)
_PRELUDE_INTLITERALS(std::int64_t,  _i64)
_PRELUDE_INTLITERALS(std::uint8_t,  _u8)
_PRELUDE_INTLITERALS(std::uint16_t, _u16)
_PRELUDE_INTLITERALS(std::uint32_t, _u32)
_PRELUDE_INTLITERALS(std::uint64_t, _u64)
_PRELUDE_INTLITERALS(std::size_t,   _sz)
#undef _PRELUDE_INTLITERALS

#endif //__PRELUDE__95403838
