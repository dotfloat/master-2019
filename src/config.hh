#ifndef __CONFIG__19251282
#define __CONFIG__19251282

#include <prelude.hh>

/* In case we're stupid */
#ifndef CC_VEC_INCLUDE
# define CC_VEC_INCLUDE <real.hh>
#endif

#ifndef CC_VEC_TRAITS
# define CC_VEC_TRAITS real::vec<double, real::Dynamic>
#endif

#ifndef CC_COST_TYPE
# define CC_COST_TYPE double
#endif

#ifndef CC_COST_FN
# define CC_COST_FN ::cost::kmeans
#endif

/* Includes */
#include CC_VEC_INCLUDE
#include "util/result.hh"
#include "common/cost.hh"

/* Current configuration */
namespace cc {
  using traits    = CC_VEC_TRAITS;
  using cost      = CC_COST_TYPE;
  using dim       = typename traits::dim_type;
  using unit      = typename traits::unit_type;
  using vec       = typename traits::vec_type;
  using ref       = typename traits::ref_type;
  using set       = typename traits::set_type;
  using const_ref = typename traits::const_ref_type;

  using result    = util::result_cost<set, cost>;
}

#endif //__CONFIG__19251282
