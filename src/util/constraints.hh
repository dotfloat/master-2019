#ifndef __CONSTRAINTS__86913528
#define __CONSTRAINTS__86913528

#include <vector>
#include <cassert>
#include <stdexcept>
#include <iostream>

#include <bin.hh>
#include <config.hh>

namespace uti {
  inline std::vector<size_t> make_constraints(size_t r)
  {
      assert(r < 64);

      size_t k = 1 << r;
      std::vector<size_t> c(k, 0);
      for (size_t i {}; i < k; ++i) {
          for (size_t j {}; j < k; ++j) {
              if (std::is_same_v<C_BIN_AOP, std::bit_xor<size_t>>) {
                  c[i] |= (__builtin_popcountll(i & j) & 1) << j;
              } else if (std::is_same_v<C_BIN_AOP, std::bit_or<size_t>>) {
                  c[i] |= (__builtin_popcountll(i & j) > 0) << j;
              } else {
                  std::abort();
              }
          }
      }
      return c;
  }

  class constraints {
      size_t m_width;
      std::vector<std::vector<size_t>> m_vset;

  public:
      constraints(const constraints&) = default;
      constraints(constraints&&) = default;

      constraints(size_t r, size_t d):
          m_width(1 << r),
          m_vset(d, make_constraints(r))
      {
          assert(d >= 1);
      }

      constraints& operator=(const constraints&) = default;
      constraints& operator=(constraints&&) = default;

      template <class V>
      bool fit(size_t index, const V& v) const
      {
          assert(index < m_width);
          for (size_t i {}; i < m_vset.size(); ++i) {
              bool found {};
              for (const auto& row : m_vset[i]) {
                  if (((row >> index) & 1) == static_cast<int>(v[i])) {
                  // if (binary::unit((row >> index) & 1) == v[i]) {
                      found = true;
                      break;
                  }
              }
              if (!found) {
                  return false;
              }
          }
          return true;
      }

      template <class V>
      constraints restrict(size_t index, const V& v) const
      {
          auto cpy = *this;
          assert(index < m_width);
          for (size_t i {}; i < cpy.m_vset.size(); ++i) {
              auto& set = cpy.m_vset[i];

              for (size_t j {}; j < set.size();) {
                  auto& c = set[j];
                  if (((c >> index) & 1) != static_cast<int>(v[i])) {
                  // if (binary::unit((c >> index) & 1) != v[i]) {
                      /* The vector doesn't fit, so delete it */
                      set.erase(set.begin() + j);
                  } else {
                      /* The row fits, so keep it, but resize */
                      auto mask = (~static_cast<size_t>(0)) << index;
                      c = ((c & (mask << 1)) >> 1) | (c & (~mask));
                      ++j;
                  }
              }

              if (set.empty()) {
                  throw std::logic_error { "Set empty when restricting" };
              }
          }

          --cpy.m_width;
          return cpy;
      }

      template <class IndexIt, class CenIt>
      bool does_fit(IndexIt i_first, IndexIt i_last, CenIt c_first) const
      {
          auto n = std::distance(i_first, i_last);
          for (size_t index {}; index < m_vset.size(); ++index) {
              auto c_it = c_first;
              bool found {};
              for (auto i_it = i_first; i_it != i_last; ++i_it, ++c_it) {
                  size_t nfound {};
                  const auto &c = *c_it;
                  const auto &i = *i_it;
                  for (const auto &r : m_vset[index]) {
                      if (((r >> index) & 1) == static_cast<int>(c[index])) {
                          nfound++;
                          break;
                      }
                  }
                  if (nfound == n) {
                      found = true;
                      break;
                  }
              }
              if (!found) {
                  return false;
              }
          }
          return true;
      }

      template <class IndexIt, class CenIt>
      constraints restrict(IndexIt i_first, IndexIt i_last, CenIt c_first) const
      {
          auto cpy = *this;
          auto n = std::distance(i_first, i_last);
          assert(n <= m_width);
          for (size_t index {}; index < m_vset.size(); ++index) {
              auto& set = m_vset[index];

              /* iterate over all rows */
              for (size_t row_id {}; row_id < m_width; ++row_id) {
                  const auto& row = set[row_id];
              }
              auto c_it = c_first;
              bool found {};

              /* Check if this row is valid */
              for (auto i_it = i_first; i_it != i_last; ++i_it, ++c_it) {
                  const auto &c = *c_it;
                  const auto &i = *i_it;
                  for (const auto &r : set) {
                      // if (((r >> index) & 1) != static_cast<int>(c[index])) {
                      //     set.erase(set.begin() + j);
                      // } else {
                      //     /* The row fits, so keep it, but resize */
                      //     auto mask = (~static_cast<size_t>(0)) << index;
                      //     r = ((r & (mask << 1)) >> 1) | (r & (~mask));
                      //     ++j;
                      // }
                  }
              }

              /* If it isn't, delete it */
              if (!found) {

              }

              /* Otherwise, remove columns */
              for (auto i_it = i_first; i_it != i_last; ++i_it) {
                  const auto &i = *i_it;
                  for (const auto &r : set) {
                      // if (((r >> index) & 1) != static_cast<int>(c[index])) {
                      //     set.erase(set.begin() + j);
                      // } else {
                      //     /* The row fits, so keep it, but resize */
                      //     auto mask = (~static_cast<size_t>(0)) << index;
                      //     r = ((r & (mask << 1)) >> 1) | (r & (~mask));
                      //     ++j;
                      // }
                  }
              }
          }
          cpy.m_width -= n;
          return cpy;
      }

      cc::vec best_fit(size_t index, const cc::vec& v) const
      {
          auto u = ~v;
          for (size_t i {}; i < m_vset.size(); ++i) {
              for (const auto& set : m_vset[i]) {
                  if (((set >> index) & 1) == static_cast<int>(v[i])) {
                      u[i] = v[i];
                      break;
                  }
              }
          }
          return u;
      }

      const auto& vector() const
      { return m_vset; }

      size_t width() const
      { return m_width; }
  };
}

#endif //__CONSTRAINTS__86913528
