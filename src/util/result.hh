#ifndef __RESULT__23771867
#define __RESULT__23771867

#include <functional>

#include <boost/optional.hpp>
#include <boost/compressed_pair.hpp>

namespace util {
  template <class T, class Compare = std::less<T>>
  class result {
      boost::compressed_pair<boost::optional<T>, Compare> m_pair;

      auto& m_data()
      { return m_pair.first(); }

      const auto& m_data() const
      { return m_pair.second(); }

      Compare& m_cmp()
      { return m_pair.second(); }

  public:
      using value_type = T;

      result()              = default;
      result(const result&) = delete;
      result(result&&)      = default;

      result(T data):
          m_pair(std::move(data))
      {}

      template <class C>
      result(T data, C&& compare = C {}):
          m_pair(std::move(data), std::forward<C>(compare))
      {}

      result& operator=(const result&) = delete;
      result& operator=(result&&)      = default;

      void try_assign(T&& other)
      {
          if (m_data() && !m_cmp()(*m_data(), other))
              return;
          m_data() = std::move(other);
      }

      void try_assign(result&& other)
      {
          if (!other.m_data())
              return;
          *this = std::move(*other.m_data());
      }

      const T& get() const
      { return *m_data(); }
  };

  template <class T, class CostT, class Compare = std::less<CostT>>
  class result_cost {
      boost::optional<T> m_data {};
      boost::compressed_pair<CostT, Compare> m_pair {};

      CostT& m_cost()
      { return m_pair.first(); }

      const CostT& m_cost() const
      { return m_pair.first(); }

      Compare& m_cmp()
      { return m_pair.second(); }

      bool m_cmp(const CostT& a, const CostT& b)
      { return m_pair.second()(a, b); }

  public:
      using value_type = T;

      result_cost(const result_cost&) = default;
      result_cost(result_cost&&)      = default;

      result_cost(Compare compare = {}):
          m_data(boost::none),
          m_pair(0, compare)
      {}

      result_cost(T data, CostT cost, Compare compare = {}):
          m_data(std::move(data)),
          m_pair(cost, compare)
      {}

      result_cost& operator=(const result_cost&) = delete;
      result_cost& operator=(result_cost&&)      = default;

      bool try_assign(result_cost&& other)
      {
          if (m_data && !m_cmp(other.cost(), m_cost()))
              return false;
          m_data = std::move(other.m_data);
          m_cost() = std::move(other.m_cost());
          return true;
      }

      bool try_assign(const result_cost& other)
      {
          if (m_data && !m_cmp(other.cost(), m_cost()))
              return false;
          m_data = other.m_data;
          m_cost() = other.m_cost();
          return true;
      }

      bool try_assign(T other, const CostT& other_cost)
      {
          if (m_data && !m_cmp(other_cost, m_cost()))
              return false;
          m_data = std::move(other);
          m_cost() = other_cost;
          return true;
      }

      T& get()
      { return m_data.value(); }

      const T& get() const
      { return m_data.value(); }

      bool have() const
      { return static_cast<bool>(m_data); }

      CostT& cost()
      { return m_cost(); }

      const CostT& cost() const
      { return m_cost(); }
  };

  template <class T>
  struct is_result : std::false_type {};

  template <class T, class C>
  struct is_result<result<T, C>> : std::true_type {};

  template <class T, class CT, class C>
  struct is_result<result_cost<T, CT, C>> : std::true_type {};

  template <class T>
  constexpr auto is_result_v = is_result<T>::value;
}

#endif //__RESULT__23771867
