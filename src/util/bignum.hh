#ifndef __BIGNUM__28650267
#define __BIGNUM__28650267

#include <gmpxx.h>
#include <fmt/core.h>

using mpz = mpz_class;
using mpq = mpq_class;
using mpf = mpf_class;

/**
 * Make compatible with fmtlib
 */
namespace fmt {

template <>
struct formatter<mpz_class> {
    template <class ParseContext>
    constexpr auto parse(ParseContext& ctx)
    { return ctx.begin(); }

    template <class FormatContext>
    auto format(const mpz_class& num, FormatContext& ctx)
    { return format_to(ctx.begin(), "{}", num.get_str()); }
};

template <>
struct formatter<mpq_class> {
    template <class ParseContext>
    constexpr auto parse(ParseContext& ctx)
    { return ctx.begin(); }

    template <class FormatContext>
    auto format(const mpq_class& num, FormatContext& ctx)
    { return format_to(ctx.begin(), "{}", num.get_str()); }
};

template <>
struct formatter<mpf_class> {
    template <class ParseContext>
    constexpr auto parse(ParseContext& ctx)
    { return ctx.begin(); }

    template <class FormatContext>
    auto format(const mpf_class& num, FormatContext& ctx)
    {
        mp_exp_t expo;
        auto str = num.get_str(expo);
        return format_to(ctx.begin(), "{}e{}", str, expo);
    }
};

}

#endif //__BIGNUM__28650267
