#ifndef __REAL__80124399
#define __REAL__80124399

#include "real/basic_vec.hh"
#include "real/basic_ref.hh"
#include "real/basic_view.hh"
#include "real/basic_set.hh"
#include "real/format.hh"

#include "default_config.hh"

namespace real {
  using dim  = basic_dim<C_REAL_DIM>;
  using vec  = basic_vec<C_REAL_TYPE, C_REAL_DIM>;
  using ref  = basic_ref<C_REAL_TYPE, C_REAL_DIM>;
  using view = basic_ref<C_REAL_TYPE, C_REAL_DIM>;
  using set  = basic_set<C_REAL_TYPE, C_REAL_DIM>;
}

#endif //__REAL__80124399
