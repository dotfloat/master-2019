#ifndef __BASIC_SET__34059007
#define __BASIC_SET__34059007

#include "uti/debug.hh"
#include <algorithm>
#include <numeric>
#include <vector>

#include "traits.hh"

namespace real {
  template <class T, size_t N>
  class basic_set {
      static_assert(N % uti::packet_traits<T>::count == 0, "packet::count must divide N");

  public:
      using traits_type     = traits<T, N>;
      using dim_type        = typename traits_type::dim_type;
      using unit_type       = typename traits_type::unit_type;
      using value_type      = typename traits_type::vec_type;
      using reference       = typename traits_type::ref_type;
      using const_reference = typename traits_type::const_ref_type;

      class iterator;
      class const_iterator;

  private:
      dim_type m_dim;
      size_t m_vecsiz;
      uti::vector<unit_type> m_vec;

      /* Pad vector so that it's properly aligned */
      void m_pad(typename decltype(m_vec)::iterator first)
      {
          auto dim = static_cast<size_t>(m_dim);
          auto rem = uti::round_up(dim, uti::packet_traits<T>::count) - dim;
          m_vec.insert(first, rem, T {});
      }

  public:
      basic_set() = delete;

      basic_set(const basic_set&) = default;

      basic_set(basic_set&&) noexcept = default;

      basic_set(dim_type dim, size_t count = 0):
          m_dim(dim),
          m_vecsiz(uti::round_up(dim, uti::packet_traits<T>::count)),
          m_vec(m_vecsiz * count)
      { assert(dim > 0); }

      explicit basic_set(const std::initializer_list<value_type>& il):
          m_vecsiz(uti::round_up(dim, uti::packet_traits<T>::count)),
          m_vec(il.size() * m_vecsiz)
      {
          assert(il.begin()->dim() > 0);
          auto it1 = m_vec.begin();
          auto it2 = il.begin();

          for (; it1 != m_vec.end(); it1 += m_vecsiz, ++it2) {
              std::copy(it2->begin(), it2->end(), it1);
          }
      }

      basic_set& operator=(const basic_set&) = default;
      basic_set& operator=(basic_set&&) noexcept = default;

      /**
       * Insertion
       */
      template <class U>
      void push_back(const U& vec)
      {
          cassert_eq(dim(), std::distance(vec.begin(), vec.end()));
          m_vec.insert(m_vec.end(), vec.begin(), vec.end());
          m_pad(m_vec.end());
      }

      void push_back(std::initializer_list<unit_type> vec)
      {
          cassert_eq(dim(), vec.size());
          m_vec.insert(m_vec.end(), vec.begin(), vec.end());
          m_pad(m_vec.end());
      }

      /**
       * Reset
       */
      void clear()
      { m_vec.clear(); }

      /**
       * Data
       */
      bool empty() const
      { return m_vec.empty(); }

      /**
       * Size
       */
      dim_type dim() const
      { return m_dim; }

      size_t size() const
      {
          assert(m_dim > 0);
          cassert_eq(0, m_vec.size() % m_vecsiz);
          return m_vec.size() / m_vecsiz;
      }

      /**
       * Accessors
       */
      reference front()
      { return (*this)[0]; }

      const_reference front() const
      { return (*this)[0]; }

      reference back()
      { return (*this)[size() - 1]; }

      const_reference back() const
      { return (*this)[size() - 1]; }

      reference operator[](size_t i)
      { return { m_vec.data() + i * m_vecsiz, m_dim }; }

      const_reference operator[](size_t i) const
      { return { m_vec.data() + i * m_vecsiz, m_dim }; }

      /**
       * Comparators
       */
      bool operator==(const basic_set& other) const
      {
          cassert_eq(m_vec.size(), other.m_vec.size());
          for (size_t i {}; i < m_vec.size(); ++i) {
              auto a = m_vec[i];
              auto b = other.m_vec[i];

              if (std::abs(a - b) > std::numeric_limits<T>::epsilon())
                  return false;
          }
          return true;
      }

      bool operator!=(const basic_set& other) const
      { return !(*this == other); }

      /**
       * Iterators
       */
      iterator begin()
      { return { m_vec.data(), m_dim, m_vecsiz }; }

      const_iterator begin() const
      { return { m_vec.data(), m_dim, m_vecsiz }; }

      const_iterator cbegin() const
      { return { m_vec.data(), m_dim, m_vecsiz }; }

      iterator end()
      { return { m_vec.data() + m_vec.size(), m_dim, m_vecsiz }; }

      const_iterator end() const
      { return { m_vec.data() + m_vec.size(), m_dim, m_vecsiz }; }

      const_iterator cend() const
      { return { m_vec.data() + m_vec.size(), m_dim, m_vecsiz }; }

      /**! Compute Frobenius norm */
      unit_type norm()
      {
          uti::packet<T> val;
          auto data = reinterpret_cast<const uti::packet<T>*>(m_vec.data());
          for (size_t i {}; i < m_vec.size() / uti::packet_traits<T>::count; ++i, ++data) {
              val.madd(data[0], data[0]);
          }
          return val.sum();
          //return std::inner_product(m_vec.begin(), m_vec.end(), m_vec.begin(), unit_type {});
      }
  };

  template <class T, size_t N>
  class basic_set<T, N>::iterator {
  public:
      using traits_type       = traits<T, N>;
      using unit_type         = typename traits_type::unit_type;
      using value_type        = typename traits_type::vec_type;
      using dim_type          = typename traits_type::dim_type;
      using reference         = typename traits_type::ref_type;
      using difference_type   = std::ptrdiff_t;
      using const_reference   = std::add_const_t<reference>;
      using pointer           = unit_type *;
      using const_pointer     = const unit_type *;
      using iterator_category = std::random_access_iterator_tag;

  private:
      dim_type m_dim;
      size_t m_vecsiz;
      pointer m_iter;
      reference m_ref;

  public:
      iterator() = default;
      iterator(const iterator&) = default;
      iterator(iterator&&) noexcept = default;

      iterator(pointer iter, dim_type dim, size_t vecsiz):
          m_dim(dim),
          m_vecsiz(vecsiz),
          m_iter(iter),
          m_ref(m_iter, m_dim)
      { assert(m_dim > 0); }

      iterator& operator=(const iterator& other)
      {
          m_dim = other.m_dim;
          m_iter = other.m_iter;
          m_ref.reset(m_iter, m_dim);
          return *this;
      }

      iterator& operator=(iterator&& other) noexcept
      {
          m_dim = other.m_dim;
          m_iter = other.m_iter;
          m_ref.reset(m_iter, m_dim);
          return *this;
      }

      /* Accessors */

      reference& operator*()
      { return m_ref; }

      const_reference& operator*() const
      { return m_ref; }

      reference* operator->()
      { return &m_ref; }

      const_reference* operator->() const
      { return &m_ref; }

      /* Iterator Operators */

      iterator& operator++()
      {
          m_iter += m_vecsiz;
          m_ref.reset(m_iter);
          return *this;
      }

      iterator operator++(int)
      {
          auto copy = *this;
          ++*this;
          return copy;
      }

      iterator& operator--()
      {
          m_iter -= m_vecsiz;
          m_ref.reset(m_iter);
          return *this;
      }

      iterator operator--(int)
      {
          auto copy = *this;
          --*this;
          return copy;
      }

      iterator operator+(difference_type diff) const
      {
          auto copy = *this;
          copy += diff;
          return copy;
      }

      iterator& operator+=(difference_type diff)
      {
          m_iter += diff * m_vecsiz;
          m_ref.reset(m_iter);
          return *this;
      }

      iterator operator-(difference_type diff) const
      {
          auto copy = *this;
          copy -= diff;
          return copy;
      }

      iterator& operator-=(difference_type diff)
      {
          m_iter -= diff * m_vecsiz;
          m_ref.reset(m_iter);
          return *this;
      }

      difference_type operator-(const iterator& other) const
      { return (m_iter - other.m_iter) / m_vecsiz; }

      bool operator!=(const iterator& other) const
      { return m_iter != other.m_iter; }
  };

  template <class T, size_t N>
  class basic_set<T, N>::const_iterator {
  public:
      using traits_type       = traits<T, N>;
      using unit_type         = typename traits_type::unit_type;
      using value_type        = typename traits_type::vec_type;
      using dim_type          = typename traits_type::dim_type;
      using reference         = typename traits_type::const_ref_type;
      using difference_type   = std::ptrdiff_t;
      using const_reference   = std::add_const_t<reference>;
      using pointer           = const unit_type *;
      using const_pointer     = const unit_type *;
      using iterator_category = std::random_access_iterator_tag;

  private:
      dim_type m_dim;
      size_t m_vecsize;
      pointer m_iter;
      reference m_ref;

  public:
      const_iterator() = default;
      const_iterator(const const_iterator&) = default;
      const_iterator(const_iterator&&) noexcept = default;

      const_iterator(const iterator &other):
          m_dim(other.m_dim),
          m_iter(other.m_iter),
          m_ref(m_iter, m_dim)
      {}

      const_iterator(pointer iter, dim_type dim, size_t vecsize):
          m_dim(dim),
          m_vecsize(vecsize),
          m_iter(iter),
          m_ref(m_iter, m_dim)
      { assert(m_dim > 0); }

      const_iterator& operator=(const const_iterator& other)
      {
          m_dim  = other.m_dim;
          m_iter = other.m_iter;
          m_ref.reset(m_iter, m_dim);
          return *this;
      }

      const_iterator& operator=(const iterator& other)
      {
          m_ref.m_dim  = m_dim  = other.m_dim;
          m_ref.m_data = m_iter = other.m_iter;
          return *this;
      }

      const_iterator& operator=(const_iterator&& other) noexcept
      {
          m_dim = other.m_dim;
          m_iter = other.m_iter;
          m_ref.reset(m_iter, m_dim);
          return *this;
      }

      /* Accessors */

      const_reference& operator*() const
      { return m_ref; }

      const_reference* operator->() const
      { return &m_ref; }

      /* Iterator Operators */

      const_iterator& operator++()
      {
          m_iter += m_vecsize;
          m_ref.reset(m_iter);
          return *this;
      }

      const_iterator operator++(int)
      {
          auto copy = *this;
          ++*this;
          return copy;
      }

      const_iterator& operator--()
      {
          m_iter -= m_vecsize;
          m_ref.reset(m_iter);
          return *this;
      }

      const_iterator operator--(int)
      {
          auto copy = *this;
          --*this;
          return copy;
      }

      const_iterator operator+(difference_type diff) const
      {
          auto copy = *this;
          copy += diff;
          return copy;
      }

      const_iterator& operator+=(difference_type diff)
      {
          m_iter += diff * m_vecsize;
          m_ref.reset(m_iter);
          return *this;
      }

      const_iterator operator-(difference_type diff) const
      {
          auto copy = *this;
          copy -= diff;
          return copy;
      }

      const_iterator& operator-=(difference_type diff)
      {
          m_iter -= diff * m_vecsize;
          m_ref.reset(m_iter);
          return *this;
      }

      difference_type operator-(const iterator& other) const
      { return (m_iter - other.m_iter) / m_vecsize; }

      difference_type operator-(const const_iterator& other) const
      { return (m_iter - other.m_iter) / m_vecsize; }

      bool operator!=(const iterator& other) const
      { return m_iter != other.m_iter; }

      bool operator!=(const const_iterator& other) const
      { return m_iter != other.m_iter; }
  };
}


#endif //__BASIC_SET__34059007
