#ifndef __BASIC_VIEW__25706349
#define __BASIC_VIEW__25706349

#include <boost/compressed_pair.hpp>
#include <fmt/format.h>
#include "mixin.hh"
#include "traits.hh"

template <class>
class vec_vector;

template <class>
class vec_iterator;

namespace real {
  template <class T, size_t N>
  struct mixin_types<basic_view<T, N>> {
      using traits_type            = traits<T, N>;
      using dim_type               = basic_dim<N>;
      using value_type             = T;
      using pointer                = const T*;
      using const_pointer          = const T*;
      using reference              = const T&;
      using const_reference        = const T&;
      using iterator               = const T*;
      using const_iterator         = const T*;
      using reverse_iterator       = std::reverse_iterator<iterator>;
      using const_reverse_iterator = std::reverse_iterator<const_iterator>;
  };

/**
 * euclidean::vec basic_view
 */
  template <class T, size_t N>
  class basic_view : public mixin<basic_view<T, N>, T, N> {
  public:
      using traits_type            = typename mixin_types<basic_view>::traits_type;
      using dim_type               = typename mixin_types<basic_view>::dim_type;
      using value_type             = typename mixin_types<basic_view>::value_type;
      using pointer                = typename mixin_types<basic_view>::pointer;
      using const_pointer          = typename mixin_types<basic_view>::const_pointer;
      using reference              = typename mixin_types<basic_view>::reference;
      using const_reference        = typename mixin_types<basic_view>::const_reference;
      using iterator               = typename mixin_types<basic_view>::iterator;
      using const_iterator         = typename mixin_types<basic_view>::const_iterator;
      using reverse_iterator       = typename mixin_types<basic_view>::reverse_iterator;
      using const_reverse_iterator = typename mixin_types<basic_view>::const_reverse_iterator;

  private:
      template <class, class, size_t>
      friend class mixin;

      boost::compressed_pair<pointer, dim_type> m_data;

  protected:

      /**
       * Data access
       */
      pointer p_data()
      { return m_data.first(); }

      const_pointer p_data() const
      { return m_data.first(); }

      dim_type p_dim() const
      { return m_data.second(); }

  public:
      basic_view()           = delete;
      basic_view(const basic_view&) = default;
      basic_view(basic_view&&)      = default;

      /**
       * Private constructor
       */
      basic_view(pointer data, dim_type dim):
          m_data(data, dim) {}

      template <class Vec>
      basic_view(Vec& other)
      { m_data = { other.data(), other.dim() }; }

      /**
       * Reset
       */
      void reset(pointer data)
      { m_data.first() = data; }

      void reset(pointer data, dim_type dim)
      { m_data = { data, dim }; }
  };

}

#endif //__BASIC_VIEW__25706349
