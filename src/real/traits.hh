#ifndef __TRAITS__11789813
#define __TRAITS__11789813

#include <type_traits>

#include "fwd.hh"
#include "basic_dim.hh"

namespace real {
/**
 * Define traits
 */
  template <class T, size_t N>
  struct traits {
      using unit_type      = T;
      using dim_type       = basic_dim<N>;
      using vec_type       = basic_vec<T, N>;
      using ref_type       = basic_ref<T, N>;
      using const_ref_type = basic_view<T, N>;
      using set_type       = basic_set<T, N>;
  };

  template <class>
  struct is_real : std::false_type {};

  template <class T, size_t N>
  struct is_real<basic_vec<T, N>> : std::true_type {};

  template <class T, size_t N>
  struct is_real<basic_ref<T, N>> : std::true_type {};

  template <class T, size_t N>
  struct is_real<basic_view<T, N>> : std::true_type {};

  template <class T>
  constexpr auto is_real_v = is_real<T>::value;
}

#endif //__TRAITS__11789813
