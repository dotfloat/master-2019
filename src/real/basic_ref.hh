#ifndef __BASIC_REF__56636562
#define __BASIC_REF__56636562

#include <boost/compressed_pair.hpp>
#include "mixin.hh"
#include "traits.hh"

namespace real {
  template <class T, size_t N>
  struct mixin_types<basic_ref<T, N>> {
      using traits_type            = traits<T, N>;
      using dim_type               = typename traits<T, N>::dim_type;
      using value_type             = T;
      using pointer                = T*;
      using const_pointer          = const T*;
      using reference              = T&;
      using const_reference        = const T&;
      using iterator               = T*;
      using const_iterator         = const T*;
      using reverse_iterator       = std::reverse_iterator<iterator>;
      using const_reverse_iterator = std::reverse_iterator<const_iterator>;
  };

/**
 * real::basic_ref
 */
  template <class T, size_t N>
  class basic_ref : public mixin<basic_ref<T, N>, T, N> {
  public:
      using traits_type            = typename mixin_types<basic_ref>::traits_type;
      using dim_type               = typename mixin_types<basic_ref>::dim_type;
      using value_type             = typename mixin_types<basic_ref>::value_type;
      using pointer                = typename mixin_types<basic_ref>::pointer;
      using const_pointer          = typename mixin_types<basic_ref>::const_pointer;
      using reference              = typename mixin_types<basic_ref>::reference;
      using const_reference        = typename mixin_types<basic_ref>::const_reference;
      using iterator               = typename mixin_types<basic_ref>::iterator;
      using const_iterator         = typename mixin_types<basic_ref>::const_iterator;
      using reverse_iterator       = typename mixin_types<basic_ref>::reverse_iterator;
      using const_reverse_iterator = typename mixin_types<basic_ref>::const_reverse_iterator;

  private:
      template <class, class, size_t>
      friend class mixin;

      boost::compressed_pair<pointer, dim_type> m_data;

  protected:
      /**
       * Data access
       */
      pointer p_data()
      { return m_data.first(); }

      const_pointer p_data() const
      { return m_data.first(); }

      dim_type p_dim() const
      { return m_data.second(); }

  public:
      basic_ref()                 = delete;
      basic_ref(const basic_ref&) = default;
      basic_ref(basic_ref&&)      = default;

      /**
       * Private constructor
       */
      basic_ref(pointer data, dim_type dim):
          m_data(data, dim) {}

      template <class Vec>
      basic_ref(Vec& other):
          m_data(other.data(), other.dim())
      {}

      /**
       * Assignment operators
       */
      basic_ref& operator=(const basic_ref& other)
      {
          std::copy(other.begin(), other.end(), this->begin());
          return *this;
      }

      template <class U>
      basic_ref& operator=(const U& other)
      {
          std::copy(other.begin(), other.end(), this->begin());
          return *this;
      }

      /**
       * Reset
       */
      void reset(pointer data)
      { m_data.first() = data; }

      void reset(pointer data, dim_type common)
      { m_data = { data, common }; }
  };

}

#endif //__BASIC_REF__56636562
