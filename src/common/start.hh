#ifndef __START__72541424
#define __START__72541424

#include <memory>

#include <config.hh>
#include "yaml.hh"
#include "options.hh"

namespace common {
  struct basic_algorithm {
      virtual ~basic_algorithm() {};

      virtual std::string_view name() const = 0;

      virtual cc::result trial(const cc::set& pts) = 0;

      /**
       * Called once at the end of the program, for each work thread, in order
       * of creation and non-corrurently.
       */
      virtual void post_thread() const {};

      /**
       * Called once at the end of the program, from main thread.
       */
      virtual void post_program(const yaml::dict&) const {};

      virtual std::unique_ptr<basic_algorithm> clone() const = 0;
  };

  template <class T>
  struct algorithm : basic_algorithm {
      std::unique_ptr<basic_algorithm> clone() const override
      { return std::make_unique<T>(*static_cast<const T*>(this)); }
  };

  void start(const basic_algorithm& algo);
}

#endif //__START__72541424
