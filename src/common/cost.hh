#ifndef __COST__56624755
#define __COST__56624755

#include <cassert>
#include <cstddef>
#include <string_view>

namespace cost {
  using namespace std::string_view_literals;

  struct hamming {
      static constexpr auto name = "hamming"sv;

      template <class T, class U>
      size_t operator()(const T& a, const U& b) const;
  };
}

/**
 * Hamming distance
 */
template <class T, class U>
size_t cost::hamming::operator()(const T& a, const U& b) const
{
    assert(a.size() == b.size());
    size_t cost {};
    for (size_t i {}; i < a.size(); ++i) {
        cost += (a[i] == b[i]) ? 0 : 1;
    }
    return cost;
}

#endif //__COST__56624755
