#include "algorithm.hh"
#include <uti/min.hh>
#include <bin.hh>

namespace {
  template <class AOp>
  cc::unit s_tproject(const cc::set& in_pts, cc::set& in_cts, size_t rank)
  {
      using set = bin::basic_set<0, AOp>;
      using vec = bin::basic_vec<0, AOp>;
      size_t dim = in_pts.dim();

      /*  */
      set pts(dim, in_pts.size());
      set cts(dim, in_cts.size());

      /* Copy pts */
      for (size_t i {}; i < pts.size(); ++i) {
          auto in_ref = in_pts[i];
          auto ref = pts[i];
          for (size_t j {}; j < pts.dim(); ++j) {
              ref[j] = static_cast<int>(in_ref[j]) & 1;
          }
      }

      /* Copy cts */
      for (size_t i {}; i < cts.size(); ++i) {
          auto in_ref = in_cts[i];
          auto ref = cts[i];
          for (size_t j {}; j < cts.dim(); ++j) {
              ref[j] = static_cast<int>(in_ref[j]) & 1;
          }
      }

      std::vector<std::vector<size_t>> clusters(cts.size());
      std::vector<std::pair<size_t, size_t>> costs(cts.size());
      for (size_t i {}; i < cts.size(); ++i) {
          costs[i] = { i, 0 };
      }

      /* Determine clusters */
      for (size_t i {}; i < pts.size(); ++i) {
          const auto& pt = pts[i];
          auto j = uti::argmin_pt(cts, pt);
          costs[j].second += pt.dist(cts[j]);
          clusters[j].push_back(i);
      }

      /* Find the r biggest clusters */
      std::nth_element(costs.begin(), costs.begin() + rank, costs.end(),
                       [](const auto& a, const auto& b)
                       { return a.second > b.second; });

      /* Assign copy to new_cts */
      set new_cts(cts.dim(), cts.size());
      for (size_t i {}; i < rank; ++i) {
          new_cts[i] = cts[costs[i].first];
      }

      /* Fill in the rest */
      for (size_t i = rank; i < cts.size(); ++i) {
          const auto& cpts = clusters[i];
          util::result_cost<vec, size_t> best;

          /* Iterate over all subsets */
          for (size_t s {}; s < (1_sz << rank); ++s) {
              vec new_ct(cts.dim());

              /* Create new ct */
              for (size_t j {}; j < rank; ++j) {
                  if (((s >> j) & 1_sz) == 0_sz)
                      continue;
                  new_ct = new_cts[j];
              }

              /* Compute cost */
              size_t cost {};
              for (size_t j {}; j < cpts.size(); ++j) {
                  cost += pts[j].dist(new_ct);
              }

              /* Check if best */
              best.try_assign(new_ct, cost);
          }

          new_cts[i] = new_cts[0]; //best.get();
      }

      /* Copy cts */
      for (size_t i {}; i < cts.size(); ++i) {
          auto in_ref = in_cts[i];
          auto ref = new_cts[i];
          for (size_t j {}; j < cts.dim(); ++j) {
              in_ref[j] = ref[j];
          }
      }

      return common::compute_cost(in_pts, in_cts);
  }
}

cc::unit common::project(const cc::set& in_pts, cc::set& in_cts, size_t rank)
{
    return s_tproject<std::bit_xor<size_t>>(in_pts, in_cts, rank);
}

cc::unit common::project_bool(const cc::set& in_pts, cc::set& in_cts, size_t rank)
{
    return s_tproject<std::bit_or<size_t>>(in_pts, in_cts, rank);
}
