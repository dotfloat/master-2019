#ifndef __COMPUTE_COST__20985936
#define __COMPUTE_COST__20985936

#include "config.hh"
#include "util/result.hh"

namespace common {
  template <class FwdIt>
  auto compute_cost(FwdIt first, FwdIt last, const cc::set& clusters);

  inline auto compute_cost(const cc::set& pts, const cc::set& clusters);
}

template <class FwdIt>
auto common::compute_cost(FwdIt first, FwdIt last, const cc::set& clusters)
{
    using center = util::result_cost<size_t, cc::unit>;

    cc::unit cost {};
    for (auto it = first; it != last; ++it) {
        const auto& pt = *it;
        center c;
        for (size_t i {}; i < clusters.size(); ++i) {
            const auto& ct = clusters[i];
            c.try_assign(i, ct.dist(pt));
        }
        cost += c.cost();
    }

    return cost;
}

inline auto common::compute_cost(const cc::set& pts, const cc::set& clusters)
{
    return common::compute_cost(pts.begin(), pts.end(), clusters);
}

#endif //__COMPUTE_COST__20985936
