#ifndef __DISCRETIZE__95236212
#define __DISCRETIZE__95236212

#include "util/result.hh"

namespace common {
  inline auto discretize(const cc::set& pts, cc::set& cts);
}

inline auto common::discretize(const cc::set& pts, cc::set& cts)
{
    for (auto& ct : cts) {
        util::result_cost<size_t, cc::unit> c {};
        for (size_t i {}; i < pts.size(); ++i) {
            auto pt = pts[i];
            c.try_assign(i, ct.dist(pt));
        }
        ct = pts[c.get()];
    }

    cc::unit cost {};
    for (const auto& ct : cts) {
        for (const auto& pt : pts) {
            cost += ct.dist(pt);
        }
    }

    return cost;
}

#endif //__DISCRETIZE__95236212
