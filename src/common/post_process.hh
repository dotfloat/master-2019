#ifndef __POST_PROCESS__48069310
#define __POST_PROCESS__48069310

#include "config.hh"
#include "algorithm.hh"

namespace common {
  inline void post_process(const cc::set& pts, cc::result& result);
}

//
// common::post_process
//
inline void common::post_process(const cc::set& pts, cc::result& result)
{
    // Discretise if needed
    if (g_options.discrete) {
        result.cost() = discretize(pts, result.get());
    }

    // Integralise if needed
    if (g_options.round) {
        result.cost() = round(pts, result.get());
    }

    /* Project if needed */
    if (g_options.project) {
        result.cost() = project(pts, result.get(), g_options.rank);
    } else if (g_options.project_bool) {
        result.cost() = project_bool(pts, result.get(), g_options.rank);
    }
}


#endif //__POST_PROCESS__48069310
