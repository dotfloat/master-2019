#include <vector>
#include <memory>
#include "iformat.hh"
#include "format.hh"
#include "txt.hh"

namespace {
  std::vector<std::unique_ptr<format::IFormat>> s_formats;

  struct Init {
      Init() noexcept;
  } s_init {};
}

//
// Init::Init
//
Init::Init() noexcept
{
    s_formats.emplace_back(new format::Txt);
}

//
// format::load
//
cc::set format::load(const path& path, size_t max_dim, size_t max_pts)
{
    for (const auto& fmt : s_formats) {
        if (fmt->test(path)) {
            return fmt->read(path, max_dim, max_pts);
        }
    }

    throw std::runtime_error { "Couldn't find an appropriate loader for input file" };
}
