#ifndef __TXT__93405289
#define __TXT__93405289

#include "iformat.hh"

namespace format {
  /**!
   * This parses .txt vector specifications
   */
  class Txt : public IFormat {
  public:
      Txt() noexcept = default;

      bool test(const path& path) override;

      cc::set read(const path& path, size_t max_dim, size_t max_pts) override;
  };
}

#endif //__TXT__93405289
