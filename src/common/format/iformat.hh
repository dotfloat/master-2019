#ifndef __IFORMAT__89857454
#define __IFORMAT__89857454

#include "util/filesystem.hh"
#include <config.hh>

namespace format {
  class IFormat {
  public:
      virtual ~IFormat() {}

      virtual bool test(const path& path) = 0;

      virtual cc::set read(const path& path, size_t max_dim, size_t max_pts) = 0;
  };
}

#endif //__IFORMAT__89857454
