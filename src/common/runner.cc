#include <filesystem>
#include <pthread.h>
#include <signal.h>
#include <thread>
#include <vector>
#include <fstream>
#include <algorithm>
#include <iomanip>
#include <mutex>
#include <condition_variable>
#include <chrono>
#include <fmt/ostream.h>

#include "start.hh"
#include "options.hh"
#include "util/result.hh"
#include "post_process.hh"
#include "format.hh"

using fmt::print;
using common::g_thread_id;
using common::g_trial_id;
using common::g_options;
using namespace std::chrono_literals;

namespace {
  /**! Thread-local timeout */
  thread_local bool s_stop {};
  thread_local bool s_main_thread {};
  bool s_kill {};
  std::atomic<bool> s_stopping;
  std::vector<std::thread> s_threads {};

  struct thread_data {
      cc::result result {};
      size_t throughput {};

      /* min-max outputs */
      cc::unit min = std::numeric_limits<cc::unit>::max();
      cc::unit max = std::numeric_limits<cc::unit>::min();

      /* Signal to the main thread that we're done. */
      bool done {};
      bool do_post_thread {};

      /* Condition variable for the main thread to wait for worker thread
       * algorithm running to end. */
      std::mutex algo_mutex;
      std::condition_variable algo_cond;

      /* Condition variable for the worker to call post_thread(). */
      std::mutex post_mutex;
      std::condition_variable post_cond;
  };
  std::vector<std::unique_ptr<thread_data>> s_thread_datas {};

  void s_stop_all()
  {
      /**! Only the first thread will send a signal to all other threads to
         stop. */
      bool prev_val {};
      if (!s_stopping.compare_exchange_strong(prev_val, true))
          return;

      for (auto& thread : s_threads) {
          auto pth = thread.native_handle();
          pthread_kill(pth, SIGUSR1);
      }
  }

  void s_handle_signal(int)
  {
      s_stop = true;
      s_stop_all();
  }

  void s_thread_start(const common::basic_algorithm& algorithm, const cc::set& pts)
  {
      common::g_gen.seed(std::random_device {}());
      auto algo = algorithm.clone();

      auto& data = *s_thread_datas[g_thread_id];

      for (size_t i {}; i < g_options.num_trials; ++i) {
          auto res = algo->trial(pts);

          // if (g_options.verify) {
          //     s_verify_answer(res, pts);
          // }

          if (s_stop)
              break;

          if (g_options.pre) {
              common::post_process(pts, res);
          }

          if (data.min > res.cost())
              data.min = res.cost();
          if (data.max < res.cost())
              data.max = res.cost();

          data.result.try_assign(std::move(res));
          ++data.throughput;
          ++g_trial_id;
      }

      /* Notify main thread that we're done. */
      data.done = true;
      data.algo_cond.notify_one();

      /* Wait for main thread to tell us to do post-thread stuff. */
      while (!data.do_post_thread) {
          std::unique_lock lock { data.post_mutex };
          data.post_cond.wait_for(lock, 1s);
      }

      /* Do any post-processing */
      algo->post_thread();
  }

  size_t s_throughput_min()
  {
      auto x = std::numeric_limits<size_t>::max();
      for (const auto& data : s_thread_datas) {
          if (!data) {
              x = 0;
              continue;
          }

          if (data->throughput < x)
              x = data->throughput;
      }
      return x;
  }

  size_t s_throughput_max()
  {
      auto x = std::numeric_limits<size_t>::min();
      for (const auto& data : s_thread_datas) {
          if (!data)
              continue;

          if (data->throughput > x)
              x = data->throughput;
      }
      return x;
  }

  std::pair<size_t, double> s_throughput_avgsum()
  {
      size_t p {};
      double q {};
      for (const auto& data : s_thread_datas) {
          if (!data)
              continue;

          p += data->throughput;
          q += 1;
      }
      return { p, static_cast<double>(p) / q };
  }

  void s_write_timer(const yaml::dict& yy, const std::chrono::system_clock::time_point& start, const std::chrono::system_clock::time_point& stop)
  {
      auto y = yy.subdict("timer");
      y.value("start", start);
      y.value("stop", stop);
      y.value("diff_ms", (stop - start) / 1ms);
  }

  void s_write_throughput(const yaml::dict& yy)
  {
      auto [sum, avg] = s_throughput_avgsum();
      auto y = yy.subdict("throughput");
      y.value("min", s_throughput_min());
      y.value("max", s_throughput_max());
      y.value("avg", avg);
      y.value("total", sum);
  }

  void s_write_centroids(yaml::dict& yy, const cc::result& res)
  {
      if (g_options.no_points) {
          yy.value("result", "Disabled with --no-points switch");
          return;
      }

      auto y = yy.subarray("result");
      if (res.have()) {
          for (const auto& pt : res.get()) {
              y.inline_array(pt.begin(), pt.end());
          }
      } else {
          y.value("[]");
      }
  }
}

void common::start(const common::basic_algorithm& algo)
{
    /* We use a thread-local variable instead of std::this_thread::get_id()
     * because the latter segfaults in static compiles for whatever reason. */
    s_main_thread = true;

    /* Add SIGINT signal handler to main, so that we may Ctrl-C out and still
     * get results. */
    signal(SIGINT, [](int s) {
        if (!s_main_thread)
            return;

        if (s_kill) {
            print("Aborting.\n");
            std::quick_exit(130);
        }

        print("Stopping.\n");
        s_kill = true;
        s_handle_signal(s);
    });

    /* Add SIGUSR1 signal handler so that threads can kill each other without
     * aborting the program. */
    signal(SIGUSR1, s_handle_signal);

    print("[Reading file '{}'...]", g_options.input_file);
    std::fflush(stdout);

    auto pts = format::load(g_options.input_file, g_options.max_dim, g_options.max_pts);
    print("\tDone\n");

    print("Dim: {}\n", pts.dim());

    auto norm = pts.norm();
    print("Norm: {}\n", norm);

    /* Start timing */
    auto timer_start = std::chrono::system_clock::now();

    /* If we're using a timeout, just set the number of trials to the maximum
     * value possible. This value is *very* unlikely to be reached in our
     * lifetimes, so it's fine. */
    if (g_options.timeout) {
        g_options.num_trials = std::numeric_limits<size_t>::max();
    }

    /* Start threads and run our algorithm */
    s_thread_datas.resize(g_options.num_threads);
    for (size_t i {}; i < g_options.num_threads; ++i) {
        s_thread_datas[i] = std::make_unique<thread_data>();
        s_threads.emplace_back([&, i] {
            common::g_thread_id = i;
            s_thread_start(algo, pts);
        });
    }

    /* If we've specified a timeout, wait. */
    if (g_options.timeout) {
        std::this_thread::sleep_for(*g_options.timeout);
        s_stop_all();
    }

    /* Wait for the threads to finish running the algorithms. */
    for (auto& t : s_thread_datas) {
        if (t->done)
            continue;
        std::unique_lock lock { t->algo_mutex };
        t->algo_cond.wait_for(lock, 1s);
    }

    /* Signal each thread, in order, to do any post-thread operations. */
    for (size_t i {}; i < s_thread_datas.size(); ++i) {
        auto& d = s_thread_datas[i];
        auto& t = s_threads[i];

        fmt::print("[{}/{}] Finalise thread.\r", i + 1, s_thread_datas.size());

        /* Tell thread to continue */
        d->done = false;
        d->do_post_thread = true;
        d->post_cond.notify_all();

        /* Wait for it to finish */
        t.join();
    }
    fmt::print("\n");

    /* Pick the best result. */
    for (size_t i = 1; i < g_options.num_threads; ++i) {
        s_thread_datas[0]->result.try_assign(s_thread_datas[i]->result);
    }
    auto& result = s_thread_datas[0]->result;

    /* Postprocess. */
    if (!g_options.pre) {
        post_process(pts, result);
    }

    /* Stop timer */
    auto timer_stop = std::chrono::system_clock::now();

    auto yaml = yaml::array(g_result_fd);
    auto y = yaml.subdict();

    y.inline_array("program_args", g_args.begin(), g_args.end());
    y.value("dataset", std::filesystem::absolute(common::g_options.input_file).string());
    y.value("algorithm", algo.name());
    y.value("num_pts", pts.size());
    y.value("dim", pts.dim());

    s_write_timer(y, timer_start, timer_stop);
    s_write_throughput(y);
    s_write_centroids(y, result);

    /* Costs */
    y.value("cost", result.cost());
    y.value("norm", norm);
    y.value("normal_cost", static_cast<double>(result.cost()) / static_cast<double>(norm));

    algo.post_program(y);
}
