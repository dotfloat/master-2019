#ifndef __BASIC_ALGORITHM__32739192
#define __BASIC_ALGORITHM__32739192

#include <string_view>
#include <config.hh>

namespace common {
  struct basic_algorithm {
      virtual ~basic_algorithm() {};

      virtual std::string_view name() const = 0;

      virtual cc::result trial(const cc::set& pts) = 0;

      /**
       * Called once at the end of the program, for each work thread, in order
       * of creation and non-corrurently.
       */
      virtual void post_thread() const {};

      /**
       * Called once at the end of the program, from main thread.
       */
      virtual void post_program(const yaml::dict&) const {};

      virtual std::unique_ptr<basic_algorithm> clone() const = 0;
  };
}

#endif //__BASIC_ALGORITHM__32739192
