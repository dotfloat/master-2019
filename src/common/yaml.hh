#ifndef __YAML__48397639
#define __YAML__48397639

#include <string_view>
#include <fmt/format.h>
#include <fmt/chrono.h>
#include <fmt/time.h>

namespace common {
  extern std::FILE *g_result_fd;
}

namespace yaml {
  template <class>
  struct is_string : std::false_type {};

  template <> struct is_string<std::string> : std::true_type {};
  template <> struct is_string<std::string_view> : std::true_type {};
  template <> struct is_string<const char*> : std::true_type {};
  template <> struct is_string<char *> : std::true_type {};

  std::string safe_string(std::string_view);

  template <class T>
  decltype(auto) safe(const T& val) {
      if constexpr (is_string<T>::value) {
              return safe_string(val);
          } else {
          return val;
      }
  }

  class writer {
      std::FILE *m_fd;
      size_t m_indent;

  public:
      writer(std::FILE *fd, size_t indent = 0);

      ~writer();

      writer indent() const;

      void write_prefix() const;

      void write(std::string_view str) const;

      void operator()(std::string_view str) const
      { write(str); }
  };

  class dict;

  class array {
      writer m_writer;

  public:
      array(const writer& writer);

      ~array();

      array subarray() const;

      dict subdict() const;

      template <class T>
      void value(const T& val) const;

      template <class FwdIt>
      void inline_array(FwdIt first, FwdIt last) const;
  };

  class dict {
      writer m_writer;

  public:
      dict(const writer& writer);

      ~dict();

      array subarray(std::string_view key) const;

      dict subdict(std::string_view key) const;

      /**
       * Overload in order to get fmt to display ISO-8601 dates
       */
      template <class Clock, class Duration>
      void value(std::string_view key, const std::chrono::time_point<Clock, Duration>& tp) const;

      template <class T>
      void value(std::string_view key, const T& val) const;

      template <class FwdIt>
      void inline_array(std::string_view key, FwdIt first, FwdIt last) const;
  };
}

//
// yaml::array::value
//
template <class T>
void yaml::array::value(const T& val) const
{
    std::string str;
    if (std::is_floating_point_v<T>) {
        str = fmt::format("- {:f}\n", val);
    } else {
        str = fmt::format("- {}\n", val);
    }
    m_writer.write_prefix();
    m_writer(str);
}

//
// yaml::array::inline_array
//
template <class FwdIt>
void yaml::array::inline_array(FwdIt first, FwdIt last) const
{
    m_writer.write_prefix();
    if (first == last) {
        m_writer("- []\n");
        return;
    }
    m_writer("- [ ");
    for (auto it = first; it != last; ++it) {
        if (std::is_floating_point_v<std::decay_t<decltype(*it)>>) {
            if (it + 1 == last) {
                m_writer(fmt::format("{:f} ]\n", *it));
            } else {
                m_writer(fmt::format("{:f}, ", *it));
            }
        } else {
            if (it + 1 == last) {
                m_writer(fmt::format("{} ]\n", safe(*it)));
            } else {
                m_writer(fmt::format("{}, ", safe(*it)));
            }
        }
    }
}

//
// yaml::dict::value
//
template <class Clock, class Duration>
void yaml::dict::value(std::string_view key, const std::chrono::time_point<Clock, Duration>& tp) const
{
    char mbstr[100];
    auto time = Clock::to_time_t(tp);
    std::strftime(mbstr, sizeof(mbstr), "%Y-%m-%dT%H:%M:%S%z", std::localtime(&time));
    auto str = fmt::format("{}: {}\n", key, mbstr);
    m_writer.write_prefix();
    m_writer(str);
}

template <class T>
void yaml::dict::value(std::string_view key, const T& val) const
{
    std::string str;
    if (std::is_floating_point_v<T>) {
        str = fmt::format("{}: {:F}\n", key, safe(val));
    } else {
        str = fmt::format("{}: {}\n", key, safe(val));
    }
    m_writer.write_prefix();
    m_writer(str);
}

//
// yaml::dict::inline_array
//
template <class FwdIt>
void yaml::dict::inline_array(std::string_view key, FwdIt first, FwdIt last) const
{
    m_writer.write_prefix();
    if (first == last) {
        m_writer(fmt::format("{}: []\n", key));
        return;
    }
    m_writer(fmt::format("{}: [ ", key));
    for (auto it = first; it != last; ++it) {
        if (std::is_floating_point_v<std::decay_t<decltype(*it)>>) {
            if (it + 1 == last) {
                m_writer(fmt::format("{:f} ]\n", *it));
            } else {
                m_writer(fmt::format("{:f}, ", *it));
            }
        } else {
            if (it + 1 == last) {
                m_writer(fmt::format("{} ]\n", safe(*it)));
            } else {
                m_writer(fmt::format("{}, ", safe(*it)));
            }
        }
    }
}

#endif //__YAML__48397639
