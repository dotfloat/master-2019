#ifndef __OPTIONS__73096737
#define __OPTIONS__73096737

#include <chrono>
#include <string>
#include <string_view>
#include <optional>
#include <random>
#include <vector>
#include <boost/program_options.hpp>

extern const std::string_view g_algorithm;

namespace common {
  struct options;
  extern options g_options;
  extern std::vector<std::string_view> g_args;
  extern std::FILE *g_result_fd;
  extern thread_local std::mt19937 g_gen;
  extern thread_local size_t g_thread_id;
  extern thread_local size_t g_trial_id;

  void setup(int argc, char **argv);
  void setup(int argc, char **argv, const boost::program_options::options_description &od);
}

struct common::options {
    using duration_type = std::chrono::seconds;

    bool no_points      = false;
    bool verify         = false;
    bool dynamic        = false;
    bool discrete       = false;
    bool round          = false;
    bool project        = false;
    bool project_bool   = false;
    bool pre            = false;
    size_t num_threads  = 1;
    size_t num_trials   = 1;
    size_t num_clusters = 2;
    size_t rank         = 1;
    size_t max_dim      = std::numeric_limits<size_t>::max();
    size_t max_pts      = std::numeric_limits<size_t>::max();

    std::string input_file;
    std::optional<std::string> exact_file;
    std::string type = "int";
    std::string cost;

    std::optional<duration_type> timeout;
};


#endif //__OPTIONS__73096737
