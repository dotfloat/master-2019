#ifndef __DEBUG__49605158
#define __DEBUG__49605158

#include <string_view>
#include <fmt/core.h>

namespace uti {
  struct assert_info {
      std::string_view cond_name;
      std::string_view func;
      std::string_view file;
      size_t line;
  };

  template <class... Args>
  void cassert(bool cond, const assert_info& ai, std::string_view fmt, Args&&... args)
  {
      if (!cond) return;
      auto msg = fmt::format(fmt, std::forward<Args>(args)...);
      fmt::print(stderr, "Assertion `{}` failed in {} @ {}:{}\n", ai.cond_name, ai.func, ai.file, ai.line);
      fmt::print(stderr, "\t{}\n", msg);
      std::fflush(stderr);
      std::abort();
  }

  inline void cassert(bool cond, const assert_info& ai)
  {
      if (!cond) return;
      fmt::print(stderr, "Assertion `{}` failed in {} @ {}:{}\n", ai.cond_name, ai.func, ai.file, ai.line);
      std::fflush(stderr);
      std::abort();
  }

  template <class T, class U>
  inline void cassert_eq(const T& expect, const U& actual, const assert_info& ai)
  {
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-compare"

      if (expect == actual) return;
      fmt::print(stderr, "Assertion `{}` failed in {} @ {}:{}\n", ai.cond_name, ai.func, ai.file, ai.line);
      fmt::print(stderr, "Expected:\n\t{}\n", expect);
      fmt::print(stderr, "Actual:\n\t{}\n", actual);
      std::fflush(stderr);
      std::abort();

#pragma GCC diagnostic pop
  }
}

#define cinfo(_Val) ::fmt::print(stderr, "\t" #_Val ": {}\n", _Val)
#define cassert(_Cond, ...) ::uti::cassert(_Cond, { #_Cond, __FUNCTION__, __FILE__, __LINE__ })
#define cassert_eq(_Expect, _Actual) ::uti::cassert_eq(_Expect, _Actual, { #_Expect " == " #_Actual, __FUNCTION__, __FILE__, __LINE__ })

#endif //__DEBUG__49605158
