#ifndef __PACKET__99626259
#define __PACKET__99626259

#include <algorithm>
#include <immintrin.h>
#include <vector>
#include <boost/align/aligned_allocator.hpp>
#include "helper.hh"

namespace uti {
  template <class T>
  struct packet_traits {
      static constexpr size_t count = 1;
      static constexpr size_t align = alignof(T);
  };

#ifdef __AVX__
  template <>
  struct packet_traits<double> {
      static constexpr size_t count = 4;
      static constexpr size_t align = 32;
  };

  template <>
  struct packet_traits<float> {
      static constexpr size_t count = 8;
      static constexpr size_t align = 32;
  };

  template <>
  struct packet_traits<int> {
      static constexpr size_t count = 8;
      static constexpr size_t align = 32;
  };
#elif __SSE__
  template <>
  struct packet_traits<double> {
      static constexpr size_t count = 2;
      static constexpr size_t align = 16;
  };

  template <>
  struct packet_traits<float> {
      static constexpr size_t count = 4;
      static constexpr size_t align = 16;
  };

  template <>
  struct packet_traits<int> {
      static constexpr size_t count = 4;
      static constexpr size_t align = 16;
  };
#endif

  /**
   * Implementation of packet with generic type. Hopefully the compiler will be
   * able to sort this out automatically.
   */
  template <class T>
  class packet {
      alignas(packet_traits<T>::align) T m_data[packet_traits<T>::count] {};

      template <class F>
      packet m_transform_copy(const packet& other, F&& f) const
      {
          auto c = *this;
          for (size_t i {}; i < packet_traits<T>::count; ++i) {
              c.m_data[i] = f(c.m_data[i], other.m_data[i]);
          }
          return c;
      }

      template <class F>
      packet m_transform_copy(F&& f) const
      {
          auto c = *this;
          for (size_t i {}; i < packet_traits<T>::count; ++i) {
              c.m_data[i] = f(c.m_data[i]);
          }
          return c;
      }

  public:
      packet() = default;
      packet(const packet&) = default;
      packet& operator=(const packet&) = default;

      size_t size() const
      { return packet_traits<T>::count; }

      T& operator[](size_t i)
      { return m_data[i]; }

      const T& operator[](size_t i) const
      { return m_data[i]; }

      packet &load(const double* other)
      {
          for (size_t i {}; i < size(); ++i)
              m_data[i] = other[i];
          return *this;
      }

      packet& madd(const packet& a, const packet& b)
      {
          for (size_t i {}; i < size(); ++i)
              m_data[i] += a.m_data[i] * b.m_data[i];
          return *this;
      }

      packet operator+(const packet& other) const
      { return m_transform_copy(other, std::plus<T> {}); }

      packet operator-(const packet& other) const
      { return m_transform_copy(other, std::minus<T> {}); }

      packet operator*(const T& scalar) const
      { return m_transform_copy([scalar](const T& x){ return x * scalar; }); }

      packet operator/(const T& scalar) const
      { return m_transform_copy([scalar](const T& x){ return x / scalar; }); }

      packet operator-() const
      { return m_transform_copy([](const T& x) { return -x; }); }

      packet operator!() const
      { return m_transform_copy([](const T& x) { return !x; }); }

      T sum() const
      {
          T s {};
          for (size_t i {}; i < size(); ++i) {
              s += m_data[i];
          }
          return s;
      }
  };

#if __AVX__
  template <>
  class packet<double> {
      __m256d m_data {};

  public:
      packet() = default;
      packet(const packet&) = default;
      packet& operator=(const packet&) = default;

      packet(__m256d data):
          m_data(data)
      {}

      packet& load(const double* p)
      {
          m_data = _mm256_loadu_pd(p);
          return *this;
      }

      packet& madd2(const packet& a)
      {
          m_data = _mm256_fmadd_pd(a.m_data, a.m_data, m_data);
          return *this;
      }

      packet& madd(const packet& a, const packet& b)
      {
//          __asm__("vfmadd231pd %[a], %[b], %[c]" : [c] "+x" (m_data) : [a] "x" (a.m_data), [b] "x" (b.m_data));
          m_data = _mm256_fmadd_pd(a.m_data, b.m_data, m_data);
          return *this;
      }

      packet operator+(const packet& other) const
      { return _mm256_add_pd(m_data, other.m_data); }

      packet operator-(const packet& other) const
      { return _mm256_sub_pd(m_data, other.m_data); }

      packet operator*(double scalar) const
      { return _mm256_mul_pd(m_data, _mm256_set1_pd(scalar)); }

      packet operator*(const packet& other) const
      { return _mm256_mul_pd(m_data, other.m_data); }

      packet operator/(double scalar) const
      { return _mm256_div_pd(m_data, _mm256_set1_pd(scalar)); }

      packet operator-() const
      { return _mm256_sub_pd(_mm256_set1_pd(0), m_data); }

      packet operator!() const
      { std::abort(); }

      double sum() const
      {
          __m128d lo = _mm256_castpd256_pd128(m_data);
          __m128d hi = _mm256_extractf128_pd(m_data, 1);
          lo = _mm_add_pd(lo, hi);
          hi = _mm_unpackhi_pd(lo, lo);
          return _mm_cvtsd_f64(_mm_add_sd(lo, hi));
      }
  };

  template <>
  class packet<float> {
      __m256 m_data {};

  public:
      packet() = default;
      packet(const packet&) = default;
      packet& operator=(const packet&) = default;

      packet(__m256 data):
          m_data(data)
      {}

      packet& madd2(const packet& a)
      {
          m_data = _mm256_fmadd_ps(a.m_data, a.m_data, m_data);
          return *this;
      }

      packet& madd(const packet& a, const packet& b)
      {
//          __asm__("vfmadd231pd %[a], %[b], %[c]" : [c] "+x" (m_data) : [a] "x" (a.m_data), [b] "x" (b.m_data));
          m_data = _mm256_fmadd_ps(a.m_data, b.m_data, m_data);
          return *this;
      }

      packet operator+(const packet& other) const
      { return _mm256_add_ps(m_data, other.m_data); }

      packet operator-(const packet& other) const
      { return _mm256_sub_ps(m_data, other.m_data); }

      packet operator*(double scalar) const
      { return _mm256_mul_ps(m_data, _mm256_set1_ps(scalar)); }

      packet operator*(const packet& other) const
      { return _mm256_mul_ps(m_data, other.m_data); }

      packet operator/(double scalar) const
      { return _mm256_div_ps(m_data, _mm256_set1_ps(scalar)); }

      packet operator-() const
      { return _mm256_sub_ps(_mm256_set1_ps(0), m_data); }

      packet operator!() const
      { std::abort(); }

      float sum() const
      {
          auto x128 = _mm_add_ps(_mm256_extractf128_ps(m_data, 1), _mm256_castps256_ps128(m_data));
          auto x64 = _mm_add_ps(x128, _mm_movehl_ps(x128, x128));
          auto x32 = _mm_add_ss(x64, _mm_shuffle_ps(x64, x64, 0x55));
          return _mm_cvtss_f32(x32);
      }
  };
#endif // __AVX__

  struct aligned_deleter {
      void operator()(void *ptr)
      { std::free(ptr); }
  };

  template <class T>
  using unique_ptr = std::unique_ptr<T[], aligned_deleter>;

  template <class T>
  unique_ptr<T> make_unique(size_t n)
  {
      auto nbytes = uti::round_up(n, packet_traits<T>::count) * sizeof(T);
      auto ptr = std::aligned_alloc(packet_traits<T>::align, nbytes);
      std::fill_n(reinterpret_cast<unsigned char*>(ptr), nbytes, 0);
      return unique_ptr<T> { reinterpret_cast<T*>(ptr) };
  }

  template <class T>
  struct packet_allocator {
      using pointer = T*;
      using const_pointer = const T*;
      using void_pointer = void*;
      using const_void_pointer = const void*;
      using value_type = T;
      using size_type = std::size_t;
      using difference_type = std::ptrdiff_t;

      packet_allocator() = default;

      template <class U>
      explicit constexpr packet_allocator(const packet_allocator<U>&) noexcept
      {}

      [[nodiscard]]
      pointer allocate(size_type n)
      {
          auto ptr = std::aligned_alloc(packet_traits<T>::align, uti::round_up(n, packet_traits<T>::count) * sizeof(T));
          if (!ptr) throw std::bad_alloc {};
          return reinterpret_cast<pointer>(ptr);
      }

      void deallocate(pointer ptr, size_type) noexcept
      { std::free(ptr); }
  };

  template <class T, class U>
  bool operator==(const packet_allocator<T>&, const packet_allocator<U>&)
  { return true; }

  template <class T, class U>
  bool operator!=(const packet_allocator<T>&, const packet_allocator<U>&)
  { return false; }

  template <class T>
  using vector = std::vector<T, packet_allocator<T>>;
}

#endif //__PACKET__99626259
