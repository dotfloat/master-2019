#include <fmt/core.h>
#include <fmt/format.h>
#include <fmt/ostream.h>
#include <random>
#include <fstream>
#include <boost/program_options.hpp>
#include <unordered_set>
#include "util/euclidean.hh"
#include "util/vec_vector.hh"

namespace {
  std::string s_output;
  size_t s_num_points = 100;
  size_t s_num_clusters = 1;
  size_t s_dim = 8;
  double s_p = 0.25;
  size_t s_sparse {};

  class int_iterator {
      ptrdiff_t m_value;

  public:
      using value_type = ptrdiff_t;
      using difference_type = std::ptrdiff_t;
      using iterator_category = std::random_access_iterator_tag;
      using pointer = std::add_pointer_t<value_type>;
      using reference = std::add_lvalue_reference_t<value_type>;
      using const_pointer = std::add_pointer_t<std::add_const_t<size_t>>;
      using const_reference = std::add_lvalue_reference_t<std::add_const_t<size_t>>;

      int_iterator(ptrdiff_t value):
          m_value(value)
      {}

      reference operator*()
      { return m_value; }

      pointer operator->()
      { return &m_value; }

      int_iterator& operator++()
      { ++m_value; return *this; }

      int_iterator& operator+=(difference_type diff)
      { m_value += diff; return *this; }

      int_iterator& operator-=(difference_type diff)
      { m_value -= diff; return *this; }

      difference_type operator-(const int_iterator& other) const
      { return m_value - other.m_value; }
  };

  void s_print_help(const char *program_name, boost::program_options::options_description& desc)
  {
      fmt::print("Syntax: {} [OPTION...] OUTPUT", program_name);
      fmt::print("{}", desc);
  }

  void s_parse_args(int argc, char **argv)
  {
      namespace po = boost::program_options;

      po::options_description desc("Options");
      desc.add_options()
          ("help", "Produce help message")
          ("output", po::value(&s_output), "Output file")
          (",k", po::value(&s_num_clusters), "Number of clusters")
          ("rank,r", po::value(&s_rank), "Rank of the centres")
          (",n", po::value(&s_num_points), "Number of points to generate")
          (",d", po::value(&s_dim), "Rank of the vectors")
          (",p", po::value(&s_p), "");

      po::positional_options_description pos {};
      pos.add("output", 1);

      po::options_description command_line {};
      command_line.add(desc);

      po::variables_map vm {};
      po::store(
          po::command_line_parser(argc, argv)
          .options(command_line)
          .positional(pos)
          .run(),
          vm);
      po::notify(vm);

      if (vm.count("help")) {
          s_print_help(argv[0], command_line);
          std::exit(EXIT_FAILURE);
      }

      if (!vm.count("output")) {
          if (vm.count("sparse")) {
              s_output = fmt::format("bv_n{}_d{}_k{}_p{}_sparse.txt", s_num_points, s_dim, s_num_clusters, s_p, s_sparse);
          } else {
              s_output = fmt::format("bv_n{}_d{}_k{}_p{}.txt", s_num_points, s_dim, s_num_clusters, s_p);
          }
      }

      if (!vm.count("sparse") || s_sparse > s_dim) {
          s_sparse = s_dim;
      }
  }
}

int main(int argc, char **argv)
{
    s_parse_args(argc, argv);
    if (s_num_clusters == 0)
        throw std::runtime_error("Num clusters must be >= 1");

    std::ofstream file(s_output);

    using vec = euclidean::vec<int, 0>;
    using vector = vec_vector<vec>;

    auto common = euclidean::common<0> { s_dim };
    std::unordered_set<vec> result;

    fmt::print("Generating dataset\n");
    fmt::print("0 / {}\r", s_num_points);
    std::fflush(stdout);
    std::mt19937 gen(std::random_device {}());
    std::bernoulli_distribution cointoss { 0.5 };
    std::bernoulli_distribution same { s_p };
    std::uniform_int_distribution<size_t> random_cluster { 0, s_num_clusters - 1 };

    /* Generate the clusters */
    vector basis(common, s_rank);
    for (auto& b : basis) {
        for (auto& x : b) {
            x = cointoss(gen);
        }
    }

    size_t num_unique {};
    for (size_t i {}; num_unique < s_num_points; ++i) {
        /* Select a random cluster */
        const auto& c = clusters[random_cluster(gen)];

        vec v { common };
        for (size_t k {}; k < s_dim; ++k) {
            if (!sparse_mask[k] || same(gen)) {
                v[k] = c[k];
            } else {
                v[k] = !c[k];
            }
        }

        if (!result.count(v)) {
            result.insert(v);
            ++num_unique;
            fmt::print("{} / {}\r", result.size(), s_num_points);
        }
    }
    fmt::print("\n");

    fmt::print("Done generating. Writing dataset to file '{}'\n", s_output);
    for (const auto& pt : result) {
        for (size_t j {}; j + 1 < s_dim; ++j) {
            fmt::print(file, "{} ", pt[j]);
        }
        fmt::print(file, "{}\n", pt.back());
    }
}
