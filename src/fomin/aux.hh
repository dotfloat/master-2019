#ifndef __AUX__86630968
#define __AUX__86630968

#include <vector>
#include <utility>
#include "util/result.hh"

namespace kumar {
  class aux;
}

class kumar::aux {
public:
    using vector_type = cc::vector;
    using unit_type = typename vector_type::unit_type;
    using result_type = util::result_cost<vector_type, unit_type>;
    using auxspace_type = std::vector<std::pair<size_t, unit_type>>;

    aux(const vector_type& pts, size_t num_clusters, double epsilon, double should_skip):
        m_points(pts),
        m_num_clusters(num_clusters),
        m_result(pts.common_data(), num_clusters),
        m_rec_auxspace(num_clusters, auxspace_type(m_points.size())),
        m_epsilon(epsilon),
        m_should_skip(should_skip)
    {}

    const vector_type& points() const
    { return m_points; }

    auxspace_type& auxspace(size_t i)
    { return m_rec_auxspace[i]; }

    size_t num_clusters() const
    { return m_num_clusters; }

    double epsilon() const
    { return m_epsilon; }

    result_type&& move_result()
    { return std::move(m_result); }

    double skip_probability() const
    { return m_should_skip; }

private:
    const vector_type& m_points;
    size_t m_num_clusters;
    result_type m_result;
    std::vector<auxspace_type> m_rec_auxspace;
    double m_epsilon;
    double m_should_skip;
};

#endif //__AUX__86630968
