#include <string_view>
#include <string>

#include "common/start.hh"
#include "common/cost.hh"
#include "aux.hh"
#include "superset_sampling.hh"
#include "irred_clustering.hh"
#include <lloyd.hh>

using namespace std::string_view_literals;

namespace {
  size_t s_iterations = 1;
  size_t s_lloyd = 0;

  //
  // Kumar
  //
  cc::result s_algorithm(const cc::vector& points) {
      using gen = std::decay_t<decltype(common::g_gen)>;

      kumar::aux aux(points, 1 << common::g_options.num_clusters, 0.5, 0.0);
      lloyd::aux l_aux(1 << common::g_options.num_clusters, points);
      cc::vector empty(points.common_data());
      cc::result res;
      uti::constraints r { common::g_options.num_clusters, points.dim() };

      for (size_t i {}; i < s_iterations; ++i) {
          auto k_res = kumar::irred_clustering(aux, common::g_gen, kumar::bv_superset_sampling<gen>, empty, r, s_iterations, 0);
          res.try_assign(k_res);

          if (s_lloyd > 0) {
              l_aux.curr_clusters() = k_res.get();
              for (size_t j {}; j < s_lloyd; ++j) {
                  lloyd::lr_perform(l_aux, r);
              }
              res.try_assign(l_aux.result());
          }
      }

      return res;
  };

  //
  //
  //
  void s_writer(const yaml::dict& y)
  {
      y.value("iterations", s_iterations);
      y.value("lloyd", s_lloyd);
  }
}

const std::string_view g_algorithm = "kumar-r"sv;

//
// main
//
int main(int argc, char **argv)
{
    namespace po = boost::program_options;

    po::options_description desc("Kumar-R options");
    desc.add_options()
        ("iterations,i", po::value(&s_iterations), "Number of iterations per trial")
        ("lloyd,l", po::value(&s_lloyd), "Number of low-rank Lloyd iterations to perform");

    common::setup(argc, argv, desc);
    common::start(s_algorithm, s_writer);
}
