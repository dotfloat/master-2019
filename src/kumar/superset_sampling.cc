#include <kumar.hh>
#include <common/options.hh>

namespace {
  class s_sum_reduce {
  public:
      class reference {
          cc::vec& m_ref;
          const cc::set& m_pts;
      public:

          reference(cc::vec& ref, const cc::set& pts):
              m_ref(ref),
              m_pts(pts)
          {}

          reference& operator=(const std::pair<size_t, cc::unit>& x)
          {
              m_ref += m_pts[x.first];
              return *this;
          }
      };

      using value_type [[maybe_unused]]        = cc::unit;
      using difference_type [[maybe_unused]]   = std::ptrdiff_t;
      using iterator_category [[maybe_unused]] = std::forward_iterator_tag;
      using pointer [[maybe_unused]]           = void; // We don't care

  private:
      cc::vec& m_value;
      const cc::set& m_pts;

  public:
      s_sum_reduce(cc::vec& vec, const cc::set& pts):
          m_value(vec),
          m_pts(pts)
      {}

      s_sum_reduce& operator++()
      { return *this; }

      s_sum_reduce operator++(int)
      { return *this; }

      reference operator*()
      { return { m_value, m_pts }; }

      cc::vec& value()
      { return m_value; }
  };

}

cc::vec kumar::superset_sampling(aux& aux, size_t rec, size_t offset)
{
    const auto& auxspace = aux.auxspace();
    cc::vec val(aux.points().dim());
    s_sum_reduce reducer { val, aux.points() };

    auto size = static_cast<ptrdiff_t>(std::ceil(aux.num_clusters() * 2.0 / aux.epsilon()));
    size = std::min(size, std::distance(auxspace.begin() + offset, auxspace.end()));

    std::sample(auxspace.begin() + offset, auxspace.end(), reducer, size, common::g_gen);

    return { val / static_cast<cc::unit>(size) };
}
