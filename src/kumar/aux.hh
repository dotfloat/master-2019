#ifndef __AUX__86630968
#define __AUX__86630968

#include <vector>
#include <utility>
#include "util/result.hh"
#include <config.hh>

namespace kumar {
  class aux;
}

class kumar::aux {
public:
    using vector_type = cc::set;
    using unit_type = typename vector_type::unit_type;
    using result_type = util::result_cost<vector_type, unit_type>;
    using auxspace_type = std::vector<std::pair<size_t, cc::unit>>;

    aux(const vector_type& pts, size_t num_clusters, double epsilon, double should_skip):
        m_points(pts),
        m_num_clusters(num_clusters),
        m_result(pts.dim(), num_clusters),
        m_auxspace(m_points.size()),
        m_epsilon(epsilon),
        m_should_skip(should_skip)
    {
        for (size_t i {}; i < m_points.size(); ++i) {
            auto& x = m_auxspace[i];
            x.first = i;
            x.second = 0;
        }
    }

    const vector_type& points() const
    { return m_points; }

    auxspace_type& auxspace()
    { return m_auxspace; }

    size_t num_clusters() const
    { return m_num_clusters; }

    double epsilon() const
    { return m_epsilon; }

    result_type&& move_result()
    { return std::move(m_result); }

    double skip_probability() const
    { return m_should_skip; }

private:
    const vector_type& m_points;
    size_t m_num_clusters;
    result_type m_result;
    auxspace_type m_auxspace;
    double m_epsilon;
    double m_should_skip;
};

#endif //__AUX__86630968
