#include "gtest/gtest.h"
#include "util/constraints.hh"
#include "util/binary.hh"

using uti::constraints;
using vec = binary::vec<4>;

TEST(Constraints, declare)
{
    constraints c { 1, 1 };
}

TEST(Constraints, init)
{
    constraints c { 2, 5 };

    const auto& v = c.vector();
    ASSERT_EQ(0b0000, v[0][0]);
    ASSERT_EQ(0b1010, v[0][1]);
    ASSERT_EQ(0b1100, v[0][2]);
    ASSERT_EQ(0b0110, v[0][3]);

    for (const auto& u : v) {
        ASSERT_EQ(u, v[0]);
    }
}

TEST(Constraints, restrict)
{
    constraints c1 { 2, 4 };
    vec v1 { 1, 0, 1, 0 };

    // [ 0 0 0 0 ]
    // [ 0 1 0 1 ]
    // [ 0 0 1 1 ]
    // [ 0 1 1 0 ]

    ASSERT_TRUE(c1.fit(2, v1));
    auto c2 = c1.restrict(2, v1);

    const auto& v = c2.vector();

    for (const auto& u : v) {
        ASSERT_EQ(2, u.size());
    }

    ASSERT_EQ(0b100, v[0][0]);
    ASSERT_EQ(0b010, v[0][1]);
    ASSERT_EQ(0b000, v[1][0]);
    ASSERT_EQ(0b110, v[1][1]);
    ASSERT_EQ(0b100, v[2][0]);
    ASSERT_EQ(0b010, v[2][1]);
    ASSERT_EQ(0b000, v[3][0]);
    ASSERT_EQ(0b110, v[3][1]);
}

TEST(Constraints, best_fit)
{
    constraints c1 { 2, 4 };
    vec v1 { 1, 0, 1, 0 };
    vec v2 { 1, 1, 1, 1 };

    // [ 0 0 0 0 ]
    // [ 0 1 0 1 ]
    // [ 0 0 1 1 ]
    // [ 0 1 1 0 ]

    auto c2 = c1.restrict(2, v1);

    // Rows 0, 2:
    // [ 0 0 1 ]
    // [ 0 1 0 ]
    // Rows 1, 3:
    // [ 0 0 0 ]
    // [ 0 1 1 ]
    auto c3 = c2.restrict(1, v2);

    // Rows 0, 2:
    // [ 0 0 ]
    // Rows 1, 3:
    // [ 0 1 ]
    auto v3 = c3.best_fit(1, v2);

    ASSERT_EQ(0, static_cast<int>(v3[0]));
    ASSERT_EQ(1, static_cast<int>(v3[1]));
    ASSERT_EQ(0, static_cast<int>(v3[2]));
    ASSERT_EQ(1, static_cast<int>(v3[3]));
}
