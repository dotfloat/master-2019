#include <gtest/gtest.h>
#include <vector>
#include <util/subset.hh>

TEST(Subset, declare)
{
    std::vector v { 1, 3, 7 };
    subset s { v.begin(), v.end(), 2 };
}

TEST(Subset, iterate_1)
{
    std::vector v { 1, 3, 7 };
    subset s { v.begin(), v.end(), 1 };

    auto first = s.begin();
    auto last = s.end();

    ASSERT_EQ(1, (*first)[0]);
    ++first;
    ASSERT_EQ(3, (*first)[0]);
    ++first;
    ASSERT_EQ(7, (*first)[0]);
    ++first;
    ASSERT_FALSE(last != first);
}

TEST(Subset, iterate_1_for)
{
    std::vector v { 1, 3, 7 };
    std::vector<std::vector<int>> actual, expect {
        { 1 }, { 3 }, { 7 }
    };

    subset s { v.begin(), v.end(), 1 };

    for (const auto& x : s) {
        actual.emplace_back();
        for (const auto& y : x) {
            actual.back().push_back(y);
        }
    }

    ASSERT_EQ(expect, actual);
}

TEST(Subset, iterate_2_for)
{
    std::vector v { 1, 3, 7 };
    std::vector<std::vector<int>> actual, expect {
        { 1, 3 }, { 1, 7 }, { 3, 7 }
    };

    subset s { v.begin(), v.end(), 2 };

    for (const auto& x : s) {
        actual.emplace_back();
        for (const auto& y : x) {
            actual.back().push_back(y);
        }
    }

    ASSERT_EQ(expect, actual);
}

TEST(Subset, iterate_3_for)
{
    std::vector v { 1, 3, 7 };
    std::vector<std::vector<int>> actual, expect {
        { 1, 3, 7 }
    };

    subset s { v.begin(), v.end(), 3 };

    for (const auto& x : s) {
        actual.emplace_back();
        for (const auto& y : x) {
            actual.back().push_back(y);
        }
    }

    ASSERT_EQ(expect, actual);
}

TEST(Subset, choose_0)
{
    std::vector v { 1, 3, 7 };
    auto test = [&] {
        subset s { v.begin(), v.end(), 0 };
    };

    ASSERT_THROW(test(), std::runtime_error);
}

TEST(Subset, choose_gt_n)
{
    std::vector v { 1, 3, 7 };
    auto test = [&] {
        subset s { v.begin(), v.end(), 4 };
    };

    ASSERT_THROW(test(), std::runtime_error);
}
