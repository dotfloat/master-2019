#include <gtest/gtest.h>
#include <fmt/format.h>
#include "util/euclidean.hh"
#include "util/vec_vector.hh"
#include "util/bignum.hh"

using Types = ::testing::Types<
    euclidean::vec<double, 2>,
    euclidean::vec<int, 2>,
    euclidean::vec<mpz, 2>,
    euclidean::vec<mpq, 2>,
    euclidean::vec<double>,
    euclidean::vec<int>,
    euclidean::vec<mpz>,
    euclidean::vec<mpq>
    >;

using namespace std::string_view_literals;

template <class T>
class VecVector : public ::testing::Test {};

TYPED_TEST_CASE(VecVector, Types);

TYPED_TEST(VecVector, empty)
{
    using vec = TypeParam;
    using vector = vec_vector<vec>;

    vector v = typename vec::common_type { 2 };

    ASSERT_EQ(0, v.size());
    ASSERT_TRUE(v.empty());
}

TYPED_TEST(VecVector, push_back)
{
    using vec = TypeParam;
    using vector = vec_vector<vec>;

    vector v = typename vec::common_type { 2 };
    ASSERT_EQ(0, v.size());

    vec a = { 1, 2 };
    v.push_back(a);
    ASSERT_EQ(1, v.size());

    v.push_back({ 4, 5 });
    ASSERT_EQ(2, v.size());
}

TYPED_TEST(VecVector, get_element)
{
    using vec = TypeParam;
    using vector = vec_vector<vec>;

    vector v = { { 1, 2 },
                 { 2, 3 } };

    vec v1 = { 1, 2 };
    vec v2 = { 2, 3 };

    ASSERT_EQ(v[0], v1);
    ASSERT_EQ(v[1], v2);
}

TYPED_TEST(VecVector, equality)
{
    using vec = TypeParam;
    using vector = vec_vector<vec>;

    vector v = { { 1, 2 },
                 { 2, 3 } };

    /* Reflexivity */
    ASSERT_EQ(v[0], v[0]);
    ASSERT_EQ(v[1], v[1]);

    /* Symmetry */
    ASSERT_NE(v[0], v[1]);
    ASSERT_NE(v[1], v[0]);
}

TYPED_TEST(VecVector, addition)
{
    using vec = TypeParam;
    using vector = vec_vector<vec>;

    vector v = { { 1, 2 },
                 { -4, 3 } };

    vec expect = { -3, 5 };

    /* Copy operator */
    vec c = v[0] + v[1];
    ASSERT_EQ(c, expect);

    vec d = v[1] + v[0];
    ASSERT_EQ(d, expect);

    /* Assign operator */
    v[0] += v[1];
    ASSERT_EQ(v[0], expect);
}

TYPED_TEST(VecVector, subtraction)
{
    using vec = TypeParam;
    using vector = vec_vector<vec>;

    vector v = { { 1, 2 },
                 { -4, 3 } };

    vec expect_a_b = { 5, -1 };
    vec expect_b_a = { -5, 1 };

    /* Copy operator */
    vec c = v[0] - v[1];
    ASSERT_EQ(c, expect_a_b);

    vec d = v[1] - v[0];
    ASSERT_EQ(d, expect_b_a);

    /* Assign operator */
    v[0] -= v[1];
    ASSERT_EQ(v[0], expect_a_b);
}

TYPED_TEST(VecVector, scalar_multiplication)
{
    using vec = TypeParam;
    using vector = vec_vector<vec>;

    vector v = { { 1, -2 } };
    const vec expect = { 3, -6 };

    ASSERT_EQ(v[0]*3, expect);

    v[0] *= 3;
    ASSERT_EQ(v[0], expect);
}

TYPED_TEST(VecVector, scalar_division)
{
    using vec = TypeParam;
    using vector = vec_vector<vec>;

    vector v = { { 3, -6 } };
    const vec expect = { 1, -2 };

    ASSERT_EQ(v[0]/3, expect);

    v[0] /= 3;
    ASSERT_EQ(v[0], expect);
}

TYPED_TEST(VecVector, dist_sq)
{
    using vec = TypeParam;
    using vector = vec_vector<vec>;
    using unit = typename vec::value_type;

    const vector v = { { 1, 2 },
                       { 4, 3} };
    const unit expect = 10;

    auto d1 = dist_sq(v[0], v[1]);
    auto d2 = dist_sq(v[1], v[0]);
    ASSERT_EQ(d1, expect);
    ASSERT_EQ(d2, expect);
}

TYPED_TEST(VecVector, unary_minus)
{
    using vec = TypeParam;

    const vec a = { -1, 5 };
    vec expect = { 1, -5 };

    ASSERT_EQ(-a, expect);
}

#if 0

TYPED_TEST(VecVector, iterate)
{
    using vec = TypeParam;
    using vector = vec_vector<vec>;

    const vector v = { { 1, 2 },
                       { 4, 3} };

    vec sum { v.common() };
    for (const auto& pt : v) {
        sum += pt;
    }

    vec expect { 5, 5 };
    ASSERT_EQ(sum, expect );
}

TYPED_TEST(VecVector, format)
{
    using vec = TypeParam;

    const vec a = { 3, 1 };
    const auto expect = "{ 3, 1 }"sv;

    auto s = fmt::format("{}", a);
    ASSERT_EQ(s, expect);
}

#endif
