#include <gtest/gtest.h>
#include <txt_format.hh>

using namespace std::string_literals;

namespace {
  auto s_txt =
      "1 2 3\n"
      "5 3 2\n"
      "6 9 1\n"
      "1 0 5\n"s;
}

TEST(Txt, dim)
{
    std::istringstream ss(s_txt);
    auto dim = txt::read_dim<int>(ss);
    ASSERT_EQ(dim, 3);
}

TEST(Txt, vec)
{
    std::istringstream ss(s_txt);
    auto pts = txt::read_file<int, 3>(ss, size_tag<3>{ 3 });

    ASSERT_EQ(pts.dim(), 3);
    ASSERT_EQ(pts.size(), 4);
}
