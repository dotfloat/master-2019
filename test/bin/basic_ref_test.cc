#include <gtest/gtest.h>
#include <bin/format.hh>
#include <bin/basic_vec.hh>
#include <bin/basic_ref.hh>

using Types = ::testing::Types<
    bin::basic_vec<bin::Dynamic>,
    bin::basic_vec<1>
    >;

using namespace std::string_view_literals;

template <class T>
class Ref : public ::testing::Test {};

TYPED_TEST_CASE(Ref, Types);

TYPED_TEST(Ref, equality)
{
    using vec = TypeParam;
    using ref = typename vec::traits_type::ref_type;

    vec _a = { 1, 0 };
    vec _b = { 1, 1 };

    ref a = _a;
    ref b = _b;

    /* Reflexivity */
    ASSERT_EQ(a, a);
    ASSERT_EQ(b, b);

    /* Symmetry */
    ASSERT_NE(a, b);
    ASSERT_NE(b, a);
}

TYPED_TEST(Ref, ctor)
{
    using vec = TypeParam;
    using ref = typename vec::traits_type::ref_type;

    vec a = { 1, 0 };
    ref b = a;
    ref c = b;
    vec d = c;

    ASSERT_EQ(b, a);
    ASSERT_EQ(c, a);
    ASSERT_EQ(d, a);
}

TYPED_TEST(Ref, addition_subtraction)
{
    using vec = TypeParam;
    using ref = typename vec::traits_type::ref_type;

    vec _a      = { 0, 1, 0, 1 };
    vec _b      = { 0, 0, 1, 1 };
    vec expect = { 0, 1, 1, 0 };

    ref a = _a;
    ref b = _b;

    /* Copy operator */
    EXPECT_EQ(a + b, expect);
    EXPECT_EQ(b + a, expect);
    EXPECT_EQ(a - b, expect);
    EXPECT_EQ(b - a, expect);

    /* Assign operator */
    vec _a_plus_b = a, _a_minus_b = a;
    ref a_plus_b = _a_plus_b, a_minus_b = _a_minus_b;
    a_plus_b += b;
    a_minus_b -= b;

    EXPECT_EQ(a_plus_b, expect);
    EXPECT_EQ(a_minus_b, expect);
}

TYPED_TEST(Ref, scalar_multiplication)
{
    using vec = TypeParam;

    const vec a = { 0, 1, 0, 1 };
    const vec zero = { 0, 0, 0, 0 };

    EXPECT_EQ(a*bin::unit(0), zero);
    EXPECT_EQ(a*bin::unit(1), a);
}

TYPED_TEST(Ref, unary_minus)
{
    using vec = TypeParam;

    const vec a = { 0, 1 };

    EXPECT_EQ(-a, a);
}

TYPED_TEST(Ref, norm)
{
    using vec = TypeParam;

    vec a = { 1, 0 };

    ASSERT_EQ(a.norm(), 1);
}


TYPED_TEST(Ref, format)
{
    using vec = TypeParam;
    using ref = typename vec::traits_type::ref_type;

    vec _a = { 0, 1 };
    ref a = _a;
    const auto expect = "{ 0, 1 }"sv;

    auto s = fmt::format("{}", a);
    EXPECT_EQ(s, expect);
}
