#include <gtest/gtest.h>
#include <real.hh>

using Types = ::testing::Types<
    real::basic_vec<float, real::Dynamic>,
    real::basic_vec<float, 8>,
    real::basic_vec<double, real::Dynamic>,
    real::basic_vec<double, 8>
    >;

using namespace std::string_view_literals;

template <class T>
class Basic : public ::testing::Test {};

TYPED_TEST_CASE(Basic, Types);

TYPED_TEST(Basic, equality)
{
    using vec = TypeParam;

    vec a = { 23.9, 54.2 };
    const vec b = { -3.4, 12.4 };

    /* Reflexivity */
    ASSERT_EQ(a, a);
    ASSERT_EQ(b, b);

    /* Symmetry */
    ASSERT_NE(a, b);
    ASSERT_NE(b, a);
}

TYPED_TEST(Basic, addition)
{
    using vec = TypeParam;

    vec a       = { 1.5, -2.0, 3.0, -4.0 };
    const vec b = { 1.5, 2.0, 3.0, -2.0 };
    vec expect  = { 3.0, 0.0, 6.0, -6.0 };

    /* Copy operator */
    EXPECT_EQ(a + b, expect);
    EXPECT_EQ(b + a, expect);

    /* Assign operator */
    auto a_plus_b = a;
    a_plus_b += b;

    EXPECT_EQ(a_plus_b, expect);
}

TYPED_TEST(Basic, subtraction)
{
    using vec = TypeParam;

    vec a       = { 1.5, -2.0, 3.0, -4.0 };
    const vec b = { 1.5, 2.0, 3.0, -2.0 };
    vec expect  = { 0.0, -4.0, 0.0, -2.0 };

    /* Copy operator */
    EXPECT_EQ(a - b, expect);

    /* Assign operator */
    auto a_minus_b = a;
    a_minus_b -= b;

    EXPECT_EQ(a_minus_b, expect);
}

TYPED_TEST(Basic, scalar_multiplication)
{
    using vec = TypeParam;

    const vec a = { 0, 1, 0, 1 };
    const vec zero = { 0, 0, 0, 0 };

    EXPECT_EQ(a*0.0, zero);
    EXPECT_EQ(a*1.0, a);
}

TYPED_TEST(Basic, scalar_division)
{
    using vec = TypeParam;

    const vec a = { 0.0, 12.0, 48.0, -48.0 };
    const vec expect = { 0.0, 3.0, 12.0, -12.0 };

    EXPECT_EQ(a/4.0, expect);
}

TYPED_TEST(Basic, unary_minus)
{
    using vec = TypeParam;

    const vec a = { 0.0, -10.0, 54.0, -3.0 };
    const vec expect = { -0.0, 10.0, -54.0, 3.0 };

    EXPECT_EQ(-a, expect);
}

// TYPED_TEST(Basic, unary_invert)
// {
//     using vec = TypeParam;

//     const vec a = { 0, 1, 1, 0 };
//     const vec b = { 1, 0, 0, 1 };

//     EXPECT_EQ(~a, b);
// }

// TYPED_TEST(Basic, norm)
// {
//     using vec = TypeParam;

//     vec a = { 1, 0 };

//     ASSERT_EQ(a.norm(), 1);
// }

// TYPED_TEST(Basic, format)
// {
//     using vec = TypeParam;

//     const vec a = { 0, 1 };
//     const auto expect = "{ 0, 1 }"sv;

//     auto s = fmt::format("{}", a);
//     EXPECT_EQ(s, expect);
// }
