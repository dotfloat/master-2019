#include <gtest/gtest.h>
#include <real.hh>

using Types = ::testing::Types<
    real::basic_set<float, real::Dynamic>,
    real::basic_set<float, 8>,
    real::basic_set<double, real::Dynamic>,
    real::basic_set<double, 8>
    >;

template <class T>
class Set : public ::testing::Test {};

TYPED_TEST_CASE(Set, Types);

TYPED_TEST(Set, push_back)
{
    using set = TypeParam;
    using vec = typename set::traits_type::vec_type;
    using ref = typename vec::traits_type::ref_type;

    set s(4);

    vec v1 { 43.0, 12.0, 69524.523, 95.23423 };
    vec v2 { -54.423, 243.0, 143.6, 43234.02 };
    vec v3 { 0.0000234, 0.00543, -.0004, -.4324 };
    vec v4 { 0, 0, 1, 1 };
    s.push_back(v1);
    s.push_back(v2);
    s.push_back(v3);
    s.push_back(v4);

    ASSERT_EQ(s.size(), 4);
    ASSERT_EQ(s[0], v1);
    ASSERT_EQ(s[1], v2);
    ASSERT_EQ(s[2], v3);
    ASSERT_EQ(s[3], v4);
}
