#include <gtest/gtest.h>
#include <real.hh>

using Types = ::testing::Types<
    real::basic_vec<float, real::Dynamic>,
    real::basic_vec<float, 8>,
    real::basic_vec<double, real::Dynamic>,
    real::basic_vec<double, 8>
    >;

using namespace std::string_view_literals;

template <class T>
class Ref : public ::testing::Test {};

TYPED_TEST_CASE(Ref, Types);

TYPED_TEST(Ref, equality)
{
    using vec = TypeParam;
    using ref = typename vec::traits_type::ref_type;

    vec _a = { 23.9, 54.2 };
    vec _b = { -3.4, 12.4 };

    ref a = _a;
    ref b = _b;

    /* Reflexivity */
    ASSERT_EQ(a, a);
    ASSERT_EQ(b, b);

    /* Symmetry */
    ASSERT_NE(a, b);
    ASSERT_NE(b, a);
}

TYPED_TEST(Ref, ctor)
{
    using vec = TypeParam;
    using ref = typename vec::traits_type::ref_type;

    vec a = { 1.5, -2.0, 3.0, -4.0 };
    ref b = a;
    ref c = b;
    vec d = c;

    ASSERT_EQ(b, a);
    ASSERT_EQ(c, a);
    ASSERT_EQ(d, a);
}

TYPED_TEST(Ref, addition)
{
    using vec = TypeParam;
    using ref = typename vec::traits_type::ref_type;

    vec _a       = { 1.5, -2.0, 3.0, -4.0 };
    vec _b = { 1.5, 2.0, 3.0, -2.0 };
    vec expect  = { 3.0, 0.0, 6.0, -6.0 };

    ref a = _a;
    ref b = _b;

    /* Copy operator */
    EXPECT_EQ(a + b, expect);
    EXPECT_EQ(b + a, expect);

    /* Assign operator */
    vec _a_plus_b = a;
    ref a_plus_b = _a_plus_b;
    a_plus_b += b;

    EXPECT_EQ(a_plus_b, expect);
}

TYPED_TEST(Ref, subtraction)
{
    using vec = TypeParam;
    using ref = typename vec::traits_type::ref_type;

    vec _a       = { 1.5, -2.0, 3.0, -4.0 };
    vec _b = { 1.5, 2.0, 3.0, -2.0 };
    vec expect  = { 0.0, -4.0, 0.0, -2.0 };

    ref a = _a;
    ref b = _b;

    /* Copy operator */
    EXPECT_EQ(a - b, expect);

    /* Assign operator */
    vec _a_minus_b = a;
    ref a_minus_b = _a_minus_b;
    a_minus_b -= b;

    EXPECT_EQ(a_minus_b, expect);
}

TYPED_TEST(Ref, scalar_multiplication)
{
    using vec = TypeParam;
    using ref = typename vec::traits_type::ref_type;

    vec _a = { 0, 1, 0, 1 };
    const vec zero = { 0, 0, 0, 0 };

    ref a = _a;

    EXPECT_EQ(a*0.0, zero);
    EXPECT_EQ(a*1.0, a);
}

TYPED_TEST(Ref, scalar_division)
{
    using vec = TypeParam;
    using ref = typename vec::traits_type::ref_type;

    vec a = { 0.0, 12.0, 48.0, -48.0 };
    const vec expect = { 0.0, 3.0, 12.0, -12.0 };

    ref _a = a;

    EXPECT_EQ(_a/4.0, expect);
}

TYPED_TEST(Ref, unary_minus)
{
    using vec = TypeParam;
    using ref = typename vec::traits_type::ref_type;

    vec a = { 0.0, -10.0, 54.0, -3.0 };
    const vec expect = { -0.0, 10.0, -54.0, 3.0 };

    ref _a = a;

    EXPECT_EQ(-_a, expect);
}
